#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main(int argc, char **argv) {

	if (strcmp(argv[1], "android") == 0) 
	{
		int son_nuestras = 0;
		string line;
		ifstream archivo("../src/android/assets/www/js/loaderIndex.js");
		stringstream output;
		cout << "Actualizando proyecto Android para minificar" << endl;
		if(archivo.is_open())
		{
			while(archivo.good())
			{
				getline(archivo,line);
				if(strcmp(line.c_str(), "//Helpers") == 0)
				{
					son_nuestras = 1;
				}
				
				if(strcmp(line.substr(0,52).c_str(),"document.write('<script type=\"text/javascript\" src=\"") == 0 && son_nuestras == 1)
				{
					string path_raw = line.substr(52);
					char string_temp[256];
					strcpy(string_temp, path_raw.c_str());
					char* direccion = strtok(string_temp, "\"");
					
					stringstream concatemp;
					cout << "Minificando: " << direccion << endl;
					concatemp << "java -jar yuicompressor-2.4.8pre.jar -o ../src/android/assets/www/" << direccion << ".min " << "../src/android/assets/www/" << direccion;
					system(concatemp.str().c_str());
					
					stringstream direccion_nuevo;
					direccion_nuevo << "../src/android/assets/www/" << direccion << ".min";
					ifstream temporal(direccion_nuevo.str().c_str());
					string line_temporal;
					if(temporal.is_open())
					{
						while(temporal.good())
						{
							getline(temporal,line_temporal);
							output << line_temporal;
						}
					}
				}
				else
				{
					output << line << endl;
				}
			}
			cout << "cerrando archivo loader" << endl;
			ofstream output_file;
			output_file.open("../src/android/assets/www/js/loaderIndex.mintemp.js");
			output_file << output.str();
			output_file.close();
			cout << "Minificando loader" << endl;
			system("java -jar yuicompressor-2.4.8pre.jar -o ../src/android/assets/www/js/loaderIndex.min.js ../src/android/assets/www/js/loaderIndex.mintemp.js");
			archivo.close();
			
			cout << "Eliminando carpetas" << endl;
			system("rm -rf ../src/android/assets/www/js/controllers");
			system("rm -rf ../src/android/assets/www/js/models");
			system("rm -rf ../src/android/assets/www/js/helpers");
			system("rm -rf ../src/android/assets/www/js/views");
			system("rm ../src/android/assets/www/js/loaderIndex.mintemp.js");
			system("rm ../src/android/assets/www/js/loaderIndex.js");
			cout << "Renombrando loader" << endl;
			system("mv ../src/android/assets/www/js/loaderIndex.min.js ../src/android/assets/www/js/loaderIndex.js");
			
		}
	}
	
	if (strcmp(argv[1], "ios") == 0) 
	{
		int son_nuestras = 0;
		string line;
		ifstream archivo("../src/ios/www/js/loaderIndex.js");
		stringstream output;
		cout << "Actualizando proyecto Android para minificar" << endl;
		if(archivo.is_open())
		{
			while(archivo.good())
			{
				getline(archivo,line);
				if(strcmp(line.c_str(), "//Helpers") == 0)
				{
					son_nuestras = 1;
				}
				
				if(strcmp(line.substr(0,52).c_str(),"document.write('<script type=\"text/javascript\" src=\"") == 0 && son_nuestras == 1)
				{
					string path_raw = line.substr(52);
					char string_temp[256];
					strcpy(string_temp, path_raw.c_str());
					char* direccion = strtok(string_temp, "\"");
					
					stringstream concatemp;
					cout << "Minificando: " << direccion << endl;
					concatemp << "java -jar yuicompressor-2.4.8pre.jar -o ../src/ios/www/" << direccion << ".min " << "../src/ios/www/" << direccion;
					system(concatemp.str().c_str());
					
					stringstream direccion_nuevo;
					direccion_nuevo << "../src/ios/www/" << direccion << ".min";
					ifstream temporal(direccion_nuevo.str().c_str());
					string line_temporal;
					if(temporal.is_open())
					{
						while(temporal.good())
						{
							getline(temporal,line_temporal);
							output << line_temporal;
						}
					}
				}
				else
				{
					output << line << endl;
				}
			}
			cout << "cerrando archivo loader" << endl;
			ofstream output_file;
			output_file.open("../src/ios/www/js/loaderIndex.mintemp.js");
			output_file << output.str();
			output_file.close();
			cout << "Minificando loader" << endl;
			system("java -jar yuicompressor-2.4.8pre.jar -o ../src/ios/www/js/loaderIndex.min.js ../src/ios/www/js/loaderIndex.mintemp.js");
			archivo.close();
			
			cout << "Eliminando carpetas" << endl;
			system("rm -rf ../src/ios/www/js/controllers");
			system("rm -rf ../src/ios/www/js/models");
			system("rm -rf ../src/ios/www/js/helpers");
			system("rm -rf ../src/ios/www/js/views");
			system("rm ../src/ios/www/js/loaderIndex.mintemp.js");
			system("rm ../src/ios/www/js/loaderIndex.js");
			cout << "Renombrando loader" << endl;
			system("mv ../src/ios/www/js/loaderIndex.min.js ../src/ios/www/js/loaderIndex.js");
			
		}
	}
	
    return 0;
}