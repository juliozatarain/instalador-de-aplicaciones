<?php

class UsuarioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','createMany','admin','delete'),
				'users'=>array('developer@sisu.mx'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            
                $model = $this->loadModel($id);
                
                if(isset($_POST['Aplicaciones'])){
                    $model->aplicaciones = Aplicacion::model()->findAllByPk($_POST['Aplicaciones']);
                    $model->saveWithRelated('aplicaciones');
                }
                
		$this->render('view',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreateMany()
	{
		Yii::import('MultiModelForm');
 
                //$model = new UserGroup;
                //$model->title = 'Many';

                $member = new Usuario;
                $validatedMembers = array();  //ensure an empty array
                
                if(isset($_POST['Usuario']))
                {

                    if( //validate detail 
                        MultiModelForm::validate($member,$validatedMembers,$deleteItems)
                       )
                       {
                            foreach($validatedMembers as $usr){
                                            $usr->onBeforeSave();
                                    }
                         if (MultiModelForm::save($member,$validatedMembers,$deleteMembers,$masterValues)){
                                if(isset($_POST['Aplicaciones'])){
                                    $apps = Aplicacion::model()->findAllByPk($_POST['Aplicaciones']);
                                    foreach($validatedMembers as $usr){
                                            $usr->aplicaciones = $apps;
                                            $usr->saveWithRelated('aplicaciones');
                                    }
                                }
                                $this->redirect(array('admin'));
                            }
                        }
                }

                $this->render('createMany',array(
                    //'model'=>$model,
                    //submit the member and validatedItems to the widget in the edit form
                    'member'=>$member,
                    'validatedMembers' => $validatedMembers,
                ));

	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];
                        $model->onBeforeSave();
                        if(isset($_POST['Aplicaciones'])){
                            $model->aplicaciones = Aplicacion::model()->findAllByPk($_POST['Aplicaciones']);
                            $model->saveWithRelated('aplicaciones');
                        }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Usuario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Usuario']))
			$model->attributes=$_GET['Usuario'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Usuario the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Usuario::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Usuario $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
