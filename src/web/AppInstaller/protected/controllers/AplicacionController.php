<?php

class AplicacionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','index','admin','delete'),
				'users'=>array('developer@sisu.mx'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model = $this->loadModel($id);
                
                if(isset($_POST['Usuarios'])){
                    $model->usuarios = Usuario::model()->findAllByPk($_POST['Usuarios']);
                    $model->saveWithRelated('usuarios');
                }
            
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Aplicacion;
                $version = new Version;
                $model->versiones = $version;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation(array($model, $version));

		if(isset($_POST['Aplicacion']))
		{
			$model->attributes=$_POST['Aplicacion'];
                        $version->attributes=$_POST['Version'];
                        date_default_timezone_set("America/Mexico_City"); 
                        $version->fecha_version=date('Y-m-d H:i:s');
                        $model->versiones = $version;
                        $model->usuarios = Usuario::model()->findAllByPk($_POST['Usuarios']);
			if($model->saveWithRelated(array('usuarios')))
                        {
                            date_default_timezone_set("America/Mexico_City"); 
                            $version->fecha_version = date('Y-m-d H:i:s');
                            $version->aplicacion_id = $model->id;
                            $version->save();
                            $root = Yii::getPathOfAlias('webroot').Yii::getPathOfAlias('files').'/';
                            if(!file_exists($root.$model->nombre_app))
                            {
                                mkdir($root.$model->nombre_app);  
                            }
                            $file = CUploadedFile::getInstance($model,'img');
                            if (is_object($file) && get_class($file)==='CUploadedFile')
                            {
                                    $file->saveAs($root.$model->nombre_app.'/img.'.$file->getExtensionName());      
                            }
                            
                            if(!file_exists($root.$model->nombre_app.'/'.$version->version))
                            {
                                mkdir($root.$model->nombre_app.'/'.$version->version);  
                            }
                            $fileAPK = CUploadedFile::getInstance($version,'apk');
                            if (is_object($fileAPK) && get_class($fileAPK)==='CUploadedFile')
                            {
                                $fileAPK->saveAs($root.$model->nombre_app.'/'.$version->version.'/'.$model->nombre_app.'.apk');      
                            }
                            $fileIPA = CUploadedFile::getInstance($version,'ipa');
                            if (is_object($fileIPA) && get_class($fileIPA)==='CUploadedFile')
                            {
                                $fileIPA->saveAs($root.$model->nombre_app.'/'.$version->version.'/'.$model->nombre_app.'.ipa');      
                            }
                            $filePLIST = CUploadedFile::getInstance($version,'plist');
                            if (is_object($filePLIST) && get_class($filePLIST)==='CUploadedFile')
                            {
                                $filePLIST->saveAs($root.$model->nombre_app.'/'.$version->version.'/'.$model->nombre_app.'.plist');      
                            }
                            $push = new PushNotificationManager();
                            $push->sendPushForApp("Primera version de ".$model->nombre." esta lista!",$model);
                            $this->redirect(array('view','id'=>$model->id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
                        'version'=>$version,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            
                
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Aplicacion']))
		{
			$model->attributes=$_POST['Aplicacion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            
		$model = $this->loadModel($id);
                
                $root = Yii::getPathOfAlias('webroot').Yii::getPathOfAlias('files').'/';
                $dir = $root.$model->nombre_app;
                
                $model->delete();
                
                if(file_exists($dir))
                {
                    function rrmdir($dir) 
                    { 
                      foreach(glob($dir . '/*') as $file) 
                      { 
                        if(is_dir($file)) rrmdir($file); else unlink($file); 
                      } rmdir($dir); 
                    }
                    rrmdir($dir);
                }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Aplicacion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Aplicacion']))
			$model->attributes=$_GET['Aplicacion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Aplicacion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Aplicacion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Aplicacion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='aplicacion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
