<?php

class VersionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','createAppVer','UpdateAppVer','DeleteAppVer','File','index','view'),
				'users'=>array('developer@sisu.mx'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($aplicacion_id,$version)
	{
            $model = $this->loadModel($aplicacion_id,$version);
            $comms = Comentarios::model()->findAllByAttributes(array('version'=>$version,'aplicacion_id'=>$aplicacion_id));
            $fileDir = Yii::getPathOfAlias('files').'/'.$model->aplicacion->nombre_app.'/'.$model->version.'/'.$model->aplicacion->nombre_app;
            $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
            $model->apk=null;
            if(file_exists($root.'.apk'))
                    $model->apk=$model->aplicacion->nombre_app.'.apk';
            $model->ipa=null;
            if(file_exists($root.'.ipa'))
                    $model->ipa=$model->aplicacion->nombre_app.'.ipa';
            $model->plist=null;
            if(file_exists($root.'.plist'))
                    $model->plist=$model->aplicacion->nombre_app.'.plist';
                    
            
		$this->render('view',array(
			'model'=>$model,
                        'comms'=>$comms,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Version;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Version']))
		{
			$model->attributes=$_POST['Version'];
                        date_default_timezone_set("America/Mexico_City"); 
                        if(!$model->fecha_version)
                            $model->fecha_version=date('Y-m-d H:i:s');
			if($model->save())
                        {
                            $this->updateVersionFiles($model);
                            $push = new PushNotificationManager();
                            $push->sendPushForApp("Version ".$model->version." de ".$model->aplicacion->nombre." esta lista!",$model->aplicacion);
                            $this->redirect(array('view','aplicacion_id'=>$model->aplicacion_id,'version'=>$model->version));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Crear version desde view de aplicacion
	 * 
	 */
	public function actionCreateAppVer($aplicacion_id)
	{
		$model=new Version;
                $model->aplicacion_id=$aplicacion_id;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Version']))
		{
			$model->attributes=$_POST['Version'];
                        date_default_timezone_set("America/Mexico_City"); 
                        if(!$model->fecha_version)
                            $model->fecha_version=date('Y-m-d H:i:s');
			if($model->save())
                        {
                            $this->updateVersionFiles($model);
                            $push = new PushNotificationManager();
                            $push->sendPushForApp("Version ".$model->version." de ".$model->aplicacion->nombre." esta lista!",$model->aplicacion);
                            $this->redirect(array('aplicacion/view','id'=>$model->aplicacion_id));
                        }
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($aplicacion_id,$version)
	{
		$model=$this->loadModel($aplicacion_id,$version);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Version']))
		{
			$model->attributes=$_POST['Version'];
			if($model->save()){
                            $this->updateVersionFiles($model);
                            $this->redirect(array('view','aplicacion_id'=>$model->aplicacion_id,'version'=>$model->version));
                        }
				
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdateAppVer($aplicacion_id,$version)
	{
		$model=$this->loadModel($aplicacion_id,$version);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Version']))
		{
			$model->attributes=$_POST['Version'];
			if($model->save()){
                            $this->updateVersionFiles($model);
                            $this->redirect(array('aplicacion/view','id'=>$model->aplicacion_id));
                        }
				
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($aplicacion_id,$version)
	{
		$model = $this->loadModel($aplicacion_id,$version);
                
                $aplicacion = Aplicacion::model()->findByPk($model->aplicacion_id);
                
                $dir = Yii::getPathOfAlias('webroot').Yii::getPathOfAlias('files').'/'.$aplicacion->nombre_app.'/'.$model->version;
                
                $model->delete();
                
                if(file_exists($dir))
                {
                    function rrmdir($dir) 
                    { 
                      foreach(glob($dir . '/*') as $file) 
                      { 
                        if(is_dir($file)) rrmdir($file); else unlink($file); 
                      } rmdir($dir); 
                    }
                    rrmdir($dir);
                }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}
        
        /**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeleteAppVer($aplicacion_id,$version)
	{
		$model = $this->loadModel($aplicacion_id,$version);
                
                $aplicacion = Aplicacion::model()->findByPk($model->aplicacion_id);
                
                $dir = Yii::getPathOfAlias('webroot').Yii::getPathOfAlias('files').'/'.$aplicacion->nombre_app.'/'.$model->version;
                
                $model->delete();
                
                if(file_exists($dir))
                {
                    function rrmdir($dir) 
                    { 
                      foreach(glob($dir . '/*') as $file) 
                      { 
                        if(is_dir($file)) rrmdir($file); else unlink($file); 
                      } rmdir($dir); 
                    }
                    rrmdir($dir);
                }

		$this->redirect(array('aplicacion/view','id'=>$model->aplicacion_id));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Version');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
        
        /**
	 * Regresa el archivo
	 */
	public function actionFile($aplicacion_id,$version,$tipo)
	{
            $model = $this->loadModel($aplicacion_id,$version);
            $fileDir = Yii::getPathOfAlias('files').'/'.$model->aplicacion->nombre_app.'/'.$model->version.'/'.$model->aplicacion->nombre_app;
            $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
            /*Yii::app()->request->xSendFile($root.'.'.$tipo,array(
                //'saveName'=>'RequestedFile.jpg',
                'terminate'=>false,
             ));*/
            Yii::app()->request->sendFile($model->aplicacion->nombre_app.'.'.$tipo,file_get_contents($root.'.'.$tipo),null,false);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Version('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Version']))
			$model->attributes=$_GET['Version'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Version the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($aplicacion_id,$version)
	{
		$model=Version::model()->findByPk(array('aplicacion_id' => $aplicacion_id, 'version' => $version));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Version $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='version-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
	 * Crea o actualiza los archivos y carpetas de la version
	 * @param Version $model the model
	 */
	protected function updateVersionFiles($model)
	{
            $aplicacion = Aplicacion::model()->findByPk($model->aplicacion_id);
            $root = Yii::getPathOfAlias('webroot').Yii::getPathOfAlias('files').'/';
            if(!file_exists($root.$aplicacion->nombre_app.'/'.$model->version))
            {
                mkdir($root.$aplicacion->nombre_app.'/'.$model->version);  
            }
            $fileAPK = CUploadedFile::getInstance($model,'apk');
            if (is_object($fileAPK) && get_class($fileAPK)==='CUploadedFile')
            {
                $fileAPK->saveAs($root.$aplicacion->nombre_app.'/'.$model->version.'/'.$aplicacion->nombre_app.'.apk');      
            }
            $fileIPA = CUploadedFile::getInstance($model,'ipa');
            if (is_object($fileIPA) && get_class($fileIPA)==='CUploadedFile')
            {
                $fileIPA->saveAs($root.$aplicacion->nombre_app.'/'.$model->version.'/'.$aplicacion->nombre_app.'.ipa');      
            }
            $filePLIST = CUploadedFile::getInstance($model,'plist');
            if (is_object($filePLIST) && get_class($filePLIST)==='CUploadedFile')
            {
                $filePLIST->saveAs($root.$aplicacion->nombre_app.'/'.$model->version.'/'.$aplicacion->nombre_app.'.plist');      
            }
	}
}
