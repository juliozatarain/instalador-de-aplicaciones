<?php

class RestController extends Controller
{
    // Members
    /**
     * Datos de entrada de cada servicio
     */
    protected $actions = array(
        'appList' => array('token'),
        'verList' => array('token','idApp'),
        'cambiarPassword' => array('correo'),
        'version' => array('token','idApp','version'),
        'comentario' => array('token','idApp','version','comentario'),
        'nuevoPassword' => array('correo','clave','nuevaClave','nuevaClave2'),
        'login' => array('correo','clave','token','plataforma',),
        'logout' => array('token',),
    );
    
    protected $resources = array(
        'img' => array('token','appId'),
        'app' => array('token','appId','version'),
        'plist' => array('token','appId','version'),
    );
 
    
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array(
                //'postOnly + login',
            );
    }
    
    public function actionGet()
    {
        
        if(isset($this->resources[$_GET['resource']]) && $this->correctGetData($this->resources[$_GET['resource']])) //Hay datos correctos
        {
            $this->$_GET['resource'](); //Llamar accion correspondiente
            
        }
        else
        {
            //Mandar respuesta JSON
            header('Access-Control-Allow-Origin:*');
            echo CJSON::encode(array('resultado'=>-1));
        }
            
        

    }
 
    //Manager de acciones
    public function actionApi(){
        
        $response = array('resultado'=>-1); //Respuesta de error por defecto
        
        if(isset($this->actions[$_GET['action']]) && $this->correctPostData($this->actions[$_GET['action']])) //Hay datos correctos
        {
            $response = $this->$_GET['action']($response); //Llamar accion correspondiente
            
        }
        
        //Mandar respuesta JSON
        header('Access-Control-Allow-Origin:*');
        echo CJSON::encode($response); 
    }
    
    
    ////////////////////////////// Actions
    protected function plist()
    {
        if($this->validToken($_GET['token']))
        {
            $dispo = Dispositivo::model()->findByAttributes(array('token'=>$_GET['token']));

            //Obtener app
            $aplicacion = null;
            
            //Version solicitada
            $ver = Version::model()->findByAttributes(array('version'=>$_GET['version'],'aplicacion_id'=>$_GET['appId']));

            foreach($dispo->usuario->aplicaciones as $app){
                if($app->id == $ver->aplicacion_id)
                    $aplicacion = $app;
            }

            if($aplicacion != null) //App es del usuario
            {
                
                $fileDir = Yii::getPathOfAlias('files').'/'.$ver->aplicacion->nombre_app.'/'.$ver->version.'/'.$ver->aplicacion->nombre_app;
                $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
                
                $xml = simplexml_load_file($root.'.plist');
                $xml->dict->array->dict->array->dict->string[1] = 'http://sisu.mx/development/kantorovich/AppInstaller/index.php/rest/'.$_GET['token'].'/app/'.$_GET['appId'].'/'.$_GET['version'];
                $xml->asXml($root.'.plist');
                
                Yii::app()->request->sendFile($ver->aplicacion->nombre_app.'.plist',file_get_contents($root.'.plist'),null,false);
                return;
            }
            
        }
        
        //Mandar respuesta JSON
        header('Access-Control-Allow-Origin:*');
        echo CJSON::encode(array('resultado'=>1));
      
    }
    
    protected function app()
    {
        if($this->validToken($_GET['token']))
        {
            $dispo = Dispositivo::model()->findByAttributes(array('token'=>$_GET['token']));

            //Obtener app
            $aplicacion = null;
            
            //Version solicitada
            $ver = Version::model()->findByAttributes(array('version'=>$_GET['version'],'aplicacion_id'=>$_GET['appId']));

            foreach($dispo->usuario->aplicaciones as $app){
                if($app->id == $ver->aplicacion_id)
                    $aplicacion = $app;
            }

            if($aplicacion != null) //App es del usuario
            {
                $fileDir = Yii::getPathOfAlias('files').'/'.$ver->aplicacion->nombre_app.'/'.$ver->version.'/'.$ver->aplicacion->nombre_app;
                $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
                if($dispo->plataforma==0)
                    $tipo = 'ipa';
                else
                    $tipo = 'apk';
                Yii::app()->request->sendFile($ver->aplicacion->nombre_app.'.'.$tipo,file_get_contents($root.'.'.$tipo),null,false);
                return;
            }
            
        }
        
        
        //Mandar respuesta JSON
        header('Access-Control-Allow-Origin:*');
        echo CJSON::encode(array('resultado'=>1));
      
    }
    
    protected function img()
    {
        if($this->validToken($_GET['token']))
        {
            
            //Aplicaciones
            $aplicaciones = Dispositivo::model()->findByAttributes(array('token'=>$_GET['token']))->usuario->aplicaciones;

            //Obtener app
            $aplicacion = null;
            

            foreach($aplicaciones as $app){
                if($app->id == $_GET['appId'])
                    $aplicacion = $app;
            }

            if($aplicacion != null) //App es del usuario
            {
                $fileDir = Yii::getPathOfAlias('files').'/'.$aplicacion->nombre_app.'/img';
                $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
                if(file_exists($root.'.png'))
                    $tipo = '.png';
                if(file_exists($root.'.jpg'))
                    $tipo = '.jpg';
                if(file_exists($root.'.gif'))
                    $tipo = '.gif';
                Yii::app()->request->sendFile($aplicacion->nombre_app.$tipo,file_get_contents($root.$tipo),null,false);
            }
            
        }
        else if($_GET['token'] == '556391-9555')
        {
                $aplicacion = Aplicacion::model()->findByAttributes(array('id'=>$_GET['appId']));
                $fileDir = Yii::getPathOfAlias('files').'/'.$aplicacion->nombre_app.'/img';
                $root = Yii::getPathOfAlias('webroot').'/'.$fileDir;
                if(file_exists($root.'.png'))
                    $tipo = '.png';
                if(file_exists($root.'.jpg'))
                    $tipo = '.jpg';
                if(file_exists($root.'.gif'))
                    $tipo = '.gif';
                Yii::app()->request->sendFile($aplicacion->nombre_app.$tipo,file_get_contents($root.$tipo),null,false);
        }
        
        //Mandar respuesta JSON
        header('Access-Control-Allow-Origin:*');
        echo CJSON::encode(array('resultado'=>1));
      
    }
    
    protected function appList($response)
    {
            //Listar aplicaciones asociadas al usuario
        
            if($this->validToken($_POST['token'])) //Token valido
            {
                
                $response['resultado'] = 0; //No errores
            
                //Obtener aplicaciones
                $response['aplicaciones'] = array();
                $dispositivo = Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']));
                $appsId = array();
                foreach($dispositivo->usuario->aplicaciones as $app){
                    array_push($appsId,$app->id);
                }
                $response['aplicaciones'] = UltimaVersion::model()->findAllByAttributes(array('idApp'=>$appsId),new CDbCriteria(array('order'=>'ultima_ver DESC')));
               
                for($i=0;$i<count($response['aplicaciones']);$i++){
                    $fecha = explode(' ',$response['aplicaciones'][$i]->ultima_ver);
                    $response['aplicaciones'][$i]->ultima_ver = $fecha[0];
                }


            }
            else  //Token invalido
            {
                $response['resultado'] = 1;
                //$response['aplicaciones'] = array(array('idApp'=>31416,'nombre'=>'Girugamesh!!'));
            }
            
            return $response;
            
    }
    
    protected function verList($response)
    {
            //Listar versiones asociadas a una aplicacion del usuario
        
            if($this->validToken($_POST['token'])) //Token valido
            {
                
                
                
                //Aplicaciones
                $aplicaciones = Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']))->usuario->aplicaciones;
                
                //Obtener app
                $aplicacion = null;
                
                foreach($aplicaciones as $app){
                    if($app->id == $_POST['idApp'])
                        $aplicacion = $app;
                }
                
                if($aplicacion != null) //App es del usuario
                {
                    $response['resultado'] = 0;
                    
                    //Datos de aplicacion
                    $response['aplicacion'] = array(
                                                    'idApp'=>$aplicacion->id,
                                                    'nombre'=>$aplicacion->nombre,
                                                    'nombre_app'=>$aplicacion->nombre_app,
                                                );
                    
                    //Obtener versiones
                    $response['versiones'] = array();
                    $versiones = Version::model()->findAllByAttributes(array('aplicacion_id'=>$aplicacion->id),new CDbCriteria(array('order'=>'fecha_version DESC')));
                    foreach($versiones as $ver){
                        $fecha = explode(' ',$ver->fecha_version);
                        array_push($response['versiones'],array(
                                                            'version'=>$ver->version,
                                                            'fecha_version'=>$fecha[0],
                                                        ));
                        
                    }
                }
                else //App no existe o no es del usuario
                {
                    $response['resultado'] = 2;
                }


            }
            else  //Token invalido
            {
                $response['resultado'] = 1;
                //$response['versiones'] = array(array('idApp'=>31416,'nombre'=>'Girugamesh!!','version'=>'9.1','fecha_version'=>'20121212','notas'=>'girugamesh!!!'));
            }

            return $response;
    }
    
    protected function version($response)
    {
            //Listar versiones asociadas a una aplicacion del usuario
        
            if($this->validToken($_POST['token'])) //Token valido
            {
                
                
                
                //Aplicaciones
                $aplicaciones = Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']))->usuario->aplicaciones;
                
                //Obtener app
                $aplicacion = null;
                
                foreach($aplicaciones as $app){
                    if($app->id == $_POST['idApp'])
                        $aplicacion = $app;
                }
                
                //Version
                $version = Version::model()->findByAttributes(array('version'=>$_POST['version'],'aplicacion_id'=>$_POST['idApp']));
                
                if($aplicacion != null && $version != null) //App y version es del usuario
                {
                    $response['resultado'] = 0;
                    
                    //Datos de aplicacion
                    $response['aplicacion'] = array(
                                                    'idApp'=>$aplicacion->id,
                                                    'nombre'=>$aplicacion->nombre,
                                                    'nombre_app'=>$aplicacion->nombre_app,
                                                );
                    
                    //Obtener versiones
                    $fecha = explode(' ',$version->fecha_version);
                    $response['version'] = array(
                                                    'version'=>$version->version,
                                                    'fecha_version'=>$fecha[0],
                                                    'notas'=>$version->notas,
                                                );
                        
                    
                }
                else //App no existe o no es del usuario
                {
                    $response['resultado'] = 2;
                }


            }
            else  //Token invalido
            {
                $response['resultado'] = 1;
                //$response['versiones'] = array(array('idApp'=>31416,'nombre'=>'Girugamesh!!','version'=>'9.1','fecha_version'=>'20121212','notas'=>'girugamesh!!!'));
            }

            return $response;
    }
    
    
    protected function comentario($response)
    {
            //Listar versiones asociadas a una aplicacion del usuario
        
            if($this->validToken($_POST['token'])) //Token valido
            {
                
                //Dispositivo
                $dispo = Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']));
                //Aplicaciones
                $aplicaciones = $dispo->usuario->aplicaciones;
                
                //Obtener app
                $aplicacion = null;
                
                foreach($aplicaciones as $app){
                    if($app->id == $_POST['idApp'])
                        $aplicacion = $app;
                }
                
                //Version
                $version = Version::model()->findByAttributes(array('version'=>$_POST['version'],'aplicacion_id'=>$_POST['idApp']));
                
                if($aplicacion != null && $version != null) //App y version es del usuario
                {
                    $response['resultado'] = 0;
                    
                    $com = new Comentarios();
                    $com->aplicacion_id=$version->aplicacion_id;
                    $com->version=$version->version;
                    $com->comentario=$_POST['comentario'];
                    date_default_timezone_set("America/Mexico_City"); 
                    $com->fecha=date('Y-m-d H:i:s');
                    $com->plataforma = $dispo->plataforma;
                    $com->usuario_id = $dispo->usuario_id;
                    $com->save();
                    
                }
                else //App no existe o no es del usuario
                {
                    $response['resultado'] = 2;
                }


            }
            else  //Token invalido
            {
                $response['resultado'] = 1;
                //$response['versiones'] = array(array('idApp'=>31416,'nombre'=>'Girugamesh!!','version'=>'9.1','fecha_version'=>'20121212','notas'=>'girugamesh!!!'));
            }

            return $response;
    }
    
    
    protected function cambiarPassword($response)
    {

            $realUser = Usuario::model()->findByAttributes(array('email'=>$_POST['correo']));
            if(isset($realUser))
            {
                //Pass temporal
                $hash = strtoupper(md5(rand()));
                $password = substr($hash, rand(0, 22), 8);
                
                $to      = $realUser->email;
                $subject = 'SISU App Installer.Recuperacion de password.';
                $message = '
                    <html>
                    <body>
                            <br>
                            Saludos<br>
                            <br>
                            Te hemos mandado este mensaje por que se ha solicitado un cambio de password de tu cuenta en SISU App Installer.<br>
                            Por favor ingresa al app con el siguiente password temporal: <b>"'.$password.'"</b><br>
                            <br>
                            <br>
                            Si no solicitaste el cambio o lo pediste por error, ignora este mensaje.<br>
                            <br>
                            <br>
                            <br>
                            <img width="70" height="70" src="http://sisu.mx/development/kantorovich/AppInstaller/assets/img/sisu_logo_clear.png" />SISU Technologies developer team.
                    </body>
                    </html>';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: developer@sisu.mx';

                if(mail($to, $subject, $message, $headers))
                {
                    $response['resultado'] = 0; //if mail sent
                    $realUser->password_t=md5($password);
                    $realUser->save();
                }
                else
                    $response['resultado'] = 1; //if mail not sent
            }
            else
            {
                $response['resultado'] = 2; //User does not exist
            }
            
            return $response;

    }
    
    protected function muestraVersion($response)
    {
        return $response;
    }
    protected function descargaApp($response)
    {
        return $response;
    }


    protected function logout($response)
    {
        if($this->validToken($_POST['token'])) //Token valido
        {
            $response['resultado'] = 0;
            
            //Borrar registro de aplicacion.
            Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']))->delete();
            
        }
        else //Token invalido
        {
            $response['resultado'] = 1;
        }
        
        return $response;
    }
    
    protected function login($response)
    {
        $response['cambio'] = false;

        $response['resultado'] = 1;
        
        $model = new UserIdentity($_POST['correo'],$_POST['clave']);
        $auth = $model->authenticate();
        
        if($auth === UserIdentity::ERROR_NONE)
        {
            $response['resultado'] = 0;

            $realUser = Usuario::model()->findByAttributes(array('email'=>$model->username));
            $response['idUsuario'] = $realUser->id;
            $response['nombre'] = $realUser->nombre;
            $response['apellido'] = $realUser->apellido;
            
            $dispo = Dispositivo::model()->findByAttributes(array('token'=>$_POST['token']));
            if($dispo)
            {
                if($dispo->usuario_id != $realUser->id){
                    $dispo->usuario_id=$realUser->id;
                    $dispo->save();
                }
            }
            else
            {
                $dispo = new Dispositivo();
                $dispo->usuario_id = $realUser->id;
                $dispo->plataforma = $_POST['plataforma'];
                $dispo->token = $_POST['token'];
                $dispo->save();
            }
        }
        else if($auth === UserIdentity::ERROR_TEMPORAL_PASSWORD)
        {
            $response['resultado'] = 0;
            $response['cambio'] = true;
        }
        
        return $response;
    }
    
    protected function nuevoPassword($response)
    {

        $response['resultado'] = 1;
        
        $model = new UserIdentity($_POST['correo'],$_POST['clave']);
        $auth = $model->authenticate();
        
        if($auth === UserIdentity::ERROR_TEMPORAL_PASSWORD)
        {
            $response['resultado'] = 2;
            
            if($_POST['nuevaClave'] == $_POST['nuevaClave2']){
                
                $response['resultado'] = 0;
                
                $realUser = Usuario::model()->findByAttributes(array('email'=>$model->username));
                $realUser->password = $_POST['nuevaClave'];
                $realUser->password_t = null;
                $realUser->save();
                
            }
            

        }
        
        return $response;
    }
    
    protected function validToken($token){
        if(Dispositivo::model()->findByAttributes(array('token'=>$token)))
            return true;
        return false;
        
    }
    
    protected function correctPostData($dataArray){
        $result = true;
        foreach($dataArray as $data){
            if(!isset($_POST[$data])){
                $result= false;
                break;
            }
        }
        return $result;
    }
    
    protected function correctGetData($dataArray){
        $result = true;
        foreach($dataArray as $data){
            if(!isset($_GET[$data])){
                $result= false;
                break;
            }
        }
        return $result;
    }
    
}
?>
