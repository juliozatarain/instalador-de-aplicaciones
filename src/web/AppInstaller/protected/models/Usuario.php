<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $password
 * @property string $password_t
 *
 * The followings are the available model relations:
 * @property Dispositivos[] $dispositivoses
 * @property Aplicaciones[] $aplicaciones
 */
class Usuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        
        /*
         * Behaviors/Comportamientos
         * 
         */
        public function behaviors(){
            return array('ESaveRelatedBehavior' => array( //SaveRelatedBehavior guarda relaciones 1:M o M:M
             'class' => 'application.components.ESaveRelatedBehavior')
         );
        }
        
        /*
         * Antes de actualizar/guardar
         */
        public function onBeforeSave(){
            if($this->isNewRecord)
                $this->password=  md5 ($this->password);
            else
            { 
                if($this->password != Usuario::model()->findByPk($this->id)->password)
                    $this->password=  md5 ($this->password);
                if($this->password_t != Usuario::model()->findByPk($this->id)->password_t)
                    $this->password_t =  md5 ($this->password_t);
            }
        } 
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, apellido, email, password', 'required'),
                        array('email', 'unique'),
			array('nombre, apellido, email, password, password_t', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nombre, apellido, email, password, password_t', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dispositivoses' => array(self::HAS_MANY, 'Dispositivo', 'usuario_id'),
			'aplicaciones' => array(self::MANY_MANY, 'Aplicacion', 'usuarios_aplicaciones(usuario_id, aplicacion_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'email' => 'Email',
			'password' => 'Password',
			'password_t' => 'Password T',
		);
	}
        

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('password_t',$this->password_t,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}