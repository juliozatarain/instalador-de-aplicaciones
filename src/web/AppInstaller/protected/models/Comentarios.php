<?php

/**
 * This is the model class for table "comentarios".
 *
 * The followings are the available columns in table 'comentarios':
 * @property integer $aplicacion_id
 * @property string $version
 * @property string $comentario
 * @property string $fecha
 * @property integer $usuario_id
 * @property integer $plataforma
 * 
 * The followings are the available model relations:
 * @property Usuarios $usuario
 */
class Comentarios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comentarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comentarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('aplicacion_id, version, comentario, fecha, usuario_id, plataforma', 'required'),
			array('aplicacion_id, usuario_id, plataforma', 'numerical', 'integerOnly'=>true),
			array('version', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('aplicacion_id, version, comentario, fecha, usuario_id, plataforma', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'aplicacion_id' => 'Aplicacion',
			'version' => 'Version',
			'comentario' => 'Comentario',
			'fecha' => 'Fecha',
			'usuario_id' => 'Usuario',
			'plataforma' => 'Plataforma',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('aplicacion_id',$this->aplicacion_id);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('plataforma',$this->plataforma);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}