<?php

/**
 * This is the model class for table "ultimasVersiones".
 *
 * The followings are the available columns in table 'ultimasVersiones':
 * @property string $ultima_ver
 * @property integer $idApp
 * @property string $nombre
 */
class UltimaVersion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UltimaVersion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ultimasVersiones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idApp, nombre', 'required'),
			array('idApp', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>250),
			array('ultima_ver', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ultima_ver, idApp, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ultima_ver' => 'Ultima Ver',
			'idApp' => 'Id App',
			'nombre' => 'Nombre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ultima_ver',$this->ultima_ver,true);
		$criteria->compare('idApp',$this->idApp);
		$criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}