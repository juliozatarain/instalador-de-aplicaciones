<?php

/**
 * This is the model class for table "versiones".
 *
 * The followings are the available columns in table 'versiones':
 * @property integer $aplicacion_id
 * @property string $version
 * @property string $notas
 * @property string $fecha_version
 *
 * The followings are the available model relations:
 * @property Aplicacion $aplicacion
 */
class Version extends CActiveRecord
{
    
        public $apk;
	public $ipa;
	public $plist;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Version the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'versiones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('aplicacion_id, version, notas', 'required'),
			array('aplicacion_id', 'numerical', 'integerOnly'=>true),
			array('version', 'length', 'max'=>250),
                        array('version', 'UniqueAttributesValidator', 'with'=>'aplicacion_id'),
			array('notas', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('aplicacion_id, version, notas, fecha_version', 'safe', 'on'=>'search'),
                        array('version', 'match', 'pattern'=>'/^[a-zA-Z0-9_.]+$/', 'message'=>'La version solo puede contener caracteres alfanumericos, punto y guion bajo.'),
                        array('apk', 'file', 'types'=>'apk,', 'allowEmpty'=>true, 'maxFiles'=>1, 'message'=>'Debe ser un archivo .apk'),
                        array('ipa', 'file', 'types'=>'ipa,', 'allowEmpty'=>true, 'maxFiles'=>1, 'message'=>'Debe ser un archivo .ipa'),
                        array('plist', 'file', 'types'=>'plist,', 'allowEmpty'=>true, 'maxFiles'=>1, 'message'=>'Debe ser un archivo .plist'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aplicacion' => array(self::BELONGS_TO, 'Aplicacion', 'aplicacion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'aplicacion_id' => 'Aplicacion',
			'version' => 'Version',
			'notas' => 'Notas',
			'fecha_version' => 'Fecha Version',
                        'apk' => 'Subir APK',
                        'ipa' => 'Subir IPA',
                        'plist' => 'Subir PLIST',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('aplicacion_id',$this->aplicacion_id);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('notas',$this->notas,true);
		$criteria->compare('fecha_version',$this->fecha_version,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
}