<?php

/**
 * This is the model class for table "aplicaciones".
 *
 * The followings are the available columns in table 'aplicaciones':
 * @property integer $id
 * @property string $nombre
 * @property string $nombre_app
 * @property string $cliente
 *
 * The followings are the available model relations:
 * @property Usuarios[] $usuarios
 * @property Versiones[] $versiones
 */
class Aplicacion extends CActiveRecord
{
    
        
	public $img;
    
        /*
         * Behaviors/Comportamientos
         * 
         */
        public function behaviors(){
            return array('ESaveRelatedBehavior' => array( //SaveRelatedBehavior guarda relaciones 1:M o M:M
             'class' => 'application.components.ESaveRelatedBehavior')
         );
        }
        
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Aplicacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aplicaciones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, nombre_app', 'required'),
                        array('nombre_app', 'unique'),
			array('nombre, nombre_app, cliente', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nombre, nombre_app, cliente', 'safe', 'on'=>'search'),
                        array('nombre_app', 'match', 'pattern'=>'/^[a-zA-Z0-9_]+$/', 'message'=>'El nombre corto del app solo puede tener caracteres o guiones bajos.'),
                        array('img', 'file', 'types'=>'png', 'allowEmpty'=>true, 'maxFiles'=>1, 'message'=>'Debe ser un archivo .png'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarios' => array(self::MANY_MANY, 'Usuario', 'usuarios_aplicaciones(aplicacion_id, usuario_id)'),
			'versiones' => array(self::HAS_MANY, 'Version', 'aplicacion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'nombre_app' => 'Nombre corto App',
                        'img' => 'Subir Imagen',
                        'cliente' => 'Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nombre_app',$this->nombre_app,true);
                $criteria->compare('cliente',$this->cliente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}