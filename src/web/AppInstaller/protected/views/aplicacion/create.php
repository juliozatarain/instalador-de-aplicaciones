<?php
    /* @var $this AplicacionController */
    /* @var $model Aplicacion */

    $this->breadcrumbs=array(
            'Aplicaciones'=>array('index'),
            'Crear Nueva',
    );
?>

<h1>Create Aplicacion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'version'=>$version)); ?>