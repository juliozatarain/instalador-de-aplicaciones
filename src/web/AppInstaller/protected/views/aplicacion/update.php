<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
        'Aplicaciones'=>array('aplicacion/admin'),
	$model->nombre_app=>array('aplicacion/view','id'=>$model->id),
	'Editar',
);

?>

<h1>Editar app <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('editar', array('model'=>$model)); ?>