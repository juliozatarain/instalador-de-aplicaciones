<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
	'Aplicaciones'=>array('index'),
	$model->nombre_app,
);
?>

<h1>
    <?php 
        echo "<img width='100' height='100' src='http://sisu.mx/development/kantorovich/AppInstaller/index.php/rest/556391-9555/img/".$model->id."' />"
    ?>

    App <?php echo $model->nombre; ?>
</h1>

<?php echo CHtml::link(CHtml::button('Editar app'),array('update','id'=>$model->id),array('style'=>'float:right;')); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nombre_app',
                'cliente',
	),
)); ?>

<?php
$columns = array('Ver','Version','Fecha','Editar','Borrar');
$rows = array();
foreach($model->versiones as $version){
    $arr = array(
        CHtml::link(CHtml::image("../../assets/img/view.png"),array('version/view','aplicacion_id'=>$model->id,'version'=>$version->version)),
        $version->version,
        $version->fecha_version,
        CHtml::link(CHtml::image("../../assets/img/edit.png"),array('version/updateAppVer','aplicacion_id'=>$model->id,'version'=>$version->version)),
        CHtml::link(CHtml::image("../../assets/img/delete.png"),array('version/deleteAppVer','aplicacion_id'=>$model->id,'version'=>$version->version)),
        );
    array_push($rows,$arr);
}

$this->widget('htmlTableUi',array(
    'ajaxUrl'=>'site/handleHtmlTable',
    'arProvider'=>'',    
    'collapsed'=>false,
    'columns'=>$columns,
    'cssFile'=>'',
    'editable'=>false,
    'enableSort'=>true,
    'rows'=>$rows,
    'sortColumn'=>1,
    'sortOrder'=>'asc',
    'title'=>'Versiones',
));
?>

<div class="row buttons">
		<?php echo CHtml::link(CHtml::button('Nueva version'),array('version/CreateAppVer','aplicacion_id'=>$model->id)); ?>   
</div>
<br>
Usuarios:
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aplicacion-form',
	'enableAjaxValidation'=>true,
)); ?>
<div class="row">
    <?php  
        $data = array();
        foreach($model->usuarios as $usr){
            array_push($data,$usr->id);
        }
        $usrs = array() ;
         foreach(Usuario::model()->findAll() as $usr){
            $usrs[$usr->id] = $usr->nombre.' '.$usr->apellido;
        }
        echo Chosen::multiSelect('Usuarios',$data , $usrs,array('style'=>'width:100%'));
        echo CHtml::submitButton('Guardar cambios');
    
    ?>
</div>

<?php $this->endWidget(); ?>
<br>
<br>
<div class="row" style='float:right'>
    <?php echo CHtml::link(CHtml::button('Borrar app'),"#",array('submit'=>array('delete','id'=>$model->id),'confirm'=>'¿Seguro que deseas borrar el app '.$model->nombre.'?'),array('style'=>'float:right')); ?>
</div>