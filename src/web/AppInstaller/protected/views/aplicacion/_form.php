<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aplicacion-form',
	'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary(array($model,$version)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_app'); ?>
		<?php echo $form->textField($model,'nombre_app',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'nombre_app'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'cliente'); ?>
		<?php echo $form->textField($model,'cliente',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'cliente'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'img'); ?>
		<?php echo $form->fileField($model,'img',array('accept'=>"image/x-png")); ?>
		<?php echo $form->error($model,'img'); ?>
	</div>
        <br>
        Agregar usuarios:
        <div class="row">
            <?php  
                $data = array();
                $usrs = array();
                 foreach(Usuario::model()->findAll() as $usr){
                    $usrs[$usr->id] = $usr->nombre.' '.$usr->apellido;
                }
                echo Chosen::multiSelect('Usuarios',$data , $usrs,array('style'=>'width:100%'));
            ?>
        </div>
        
        <br><br><h2>Primera version</h2>

	<div class="row">
		<?php echo $form->labelEx($version,'version'); ?>
		<?php echo $form->textField($version,'version',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($version,'version'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($version,'apk'); ?>
		<?php echo $form->fileField($version,'apk',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($version,'apk'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($version,'ipa'); ?>
		<?php echo $form->fileField($version,'ipa',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($version,'ipa'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($version,'plist'); ?>
		<?php echo $form->fileField($version,'plist',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($version,'plist'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($version,'notas'); ?>
		<?php echo $form->textArea($version,'notas'); ?>
		<?php echo $form->error($version,'notas'); ?>
	</div>

        

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->