<?php
/* @var $comment Comentarios*/
?>

<div class="view">


        <b><?php echo CHtml::encode($comment->getAttributeLabel('usuario_id')); ?>:</b>
	<?php echo CHtml::encode($comment->usuario->nombre.' '.$comment->usuario->apellido); ?>
	<br />
        
        <b><?php echo CHtml::encode($comment->getAttributeLabel('plataforma')); ?>:</b>
	<?php echo CHtml::encode(($comment->plataforma==1?'Android':'iOS')); ?>
	<br />
        
	<b><?php echo CHtml::encode($comment->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($comment->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($comment->getAttributeLabel('comentario')); ?>:</b>
	<?php echo CHtml::encode($comment->comentario); ?>
	<br />


</div>