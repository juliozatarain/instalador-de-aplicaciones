<?php
/* @var $this VersionController */
/* @var $model Version */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'version-form',
	'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div class="row">
                <?php 
                    echo $form->labelEx($model,'aplicacion_id');
                    echo '<br>';
                    /*
                    $models=Aplicacion::model()->findAll();
                    $Data=array();
                    foreach($models as $model2)
                    {
                        $Data[$model2->id]=$model2->nombre;
                    }
                    echo $form->dropDownList($model,'aplicacion_id',$Data);
                    echo $form->error($model,'aplicacion_id'); 
                     *
                     */
                    echo $model->aplicacion->nombre;
                ?>
            <br><br>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'version'); ?>
		<?php echo $form->textField($model,'version',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'version'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notas'); ?>
		<?php echo $form->textArea($model,'notas',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'notas'); ?>
	</div>
        <br>
        <?php 
            if(!$model->isNewRecord)
                echo 'Actualizar alguno de estos archivos sobreescribira el existente en esta versión.';
        ?>
        <br>
        <div class="row">
		<?php echo $form->labelEx($model,'apk'); ?>
		<?php echo $form->fileField($model,'apk',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'apk'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'ipa'); ?>
		<?php echo $form->fileField($model,'ipa',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'ipa'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'plist'); ?>
		<?php echo $form->fileField($model,'plist',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'plist'); ?>
	</div>
        <br>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->