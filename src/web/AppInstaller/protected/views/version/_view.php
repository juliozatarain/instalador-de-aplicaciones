<?php
/* @var $this VersionController */
/* @var $data Version */
?>

<div class="view">


        <b><?php echo CHtml::encode($data->getAttributeLabel('version')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->version), array('view', 'aplicacion_id'=>$data->aplicacion_id,'version'=>$data->version)); ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('aplicacion_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->aplicacion->nombre_app), array('aplicacion/view', 'id'=>$data->aplicacion_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notas')); ?>:</b>
	<?php echo CHtml::encode($data->notas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_version')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_version); ?>
	<br />


</div>