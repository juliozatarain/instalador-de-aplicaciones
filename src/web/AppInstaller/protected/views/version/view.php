<?php
/* @var $this VersionController */
/* @var $model Version */
/* @var $comms Comentarioss */

$this->breadcrumbs=array(
        'Aplicaciones'=>array('aplicacion/admin'),
	$model->aplicacion->nombre_app=>array('aplicacion/view','id'=>$model->aplicacion_id),
	'Version '.$model->version,
);
?>

<h1>View Version <?php echo $model->aplicacion->nombre_app.' '.$model->version; ?></h1>

<?php 

$atributos = array(
    'version',
    array('label'=>'aplication_id','type'=>'raw','value'=>CHtml::link(CHtml::encode($model->aplicacion->nombre_app), array('aplicacion/view', 'id'=>$model->aplicacion_id))),
    'notas',
    'fecha_version',
);

if($model->apk != null)
    array_push($atributos,array('label'=>'apk','type'=>'raw','value'=>CHtml::link(CHtml::encode($model->apk), array('version/file', 'aplicacion_id'=>$model->aplicacion_id,'version'=>$model->version, 'tipo'=>'apk'))));
if($model->ipa != null)
    array_push($atributos,array('label'=>'ipa','type'=>'raw','value'=>CHtml::link(CHtml::encode($model->ipa), array('version/file', 'aplicacion_id'=>$model->aplicacion_id,'version'=>$model->version, 'tipo'=>'ipa'))));
if($model->plist != null)
    array_push($atributos,array('label'=>'plist','type'=>'raw','value'=>CHtml::link(CHtml::encode($model->plist), array('version/file', 'aplicacion_id'=>$model->aplicacion_id,'version'=>$model->version, 'tipo'=>'plist'))));


$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$atributos,
)); 

?>
<br><br>
Comentarios:
<?php


    foreach($comms as $comment){
        echo $this->renderPartial('comentario_view', array('comment'=>$comment));
    }


?>
