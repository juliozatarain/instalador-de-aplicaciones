<?php
/* @var $this VersionController */
/* @var $model Version */

$this->breadcrumbs=array(
        'Aplicaciones'=>array('aplicacion/admin'),
	$model->aplicacion->nombre_app=>array('aplicacion/view','id'=>$model->aplicacion_id),
	'Update version',
);
?>

<h1>Editar versión <?php echo $model->aplicacion->nombre_app.' '.$model->version ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>