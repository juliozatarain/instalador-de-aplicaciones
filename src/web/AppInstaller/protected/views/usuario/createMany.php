<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	'Nuevos',
);
?>

<h1>Nuevos Usuarios</h1>

<div class="form wide">
 
<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'group-form',
        'enableAjaxValidation'=>false,
)); ?>
 
<p class="note">Campos con <span class="required">*</span> son requeridos.</p>
 
<?php
    //show errorsummary at the top for all models
    //build an array of all models to check
    echo $form->errorSummary(array_merge(/*array($model),*/$validatedMembers));
?>
 
    <?php //echo $form->hiddenField($model,'title'); ?>
 
<?php
 
// see http://www.yiiframework.com/doc/guide/1.1/en/form.table
// Note: Can be a route to a config file too,
//       or create a method 'getMultiModelForm()' in the member model
    $apps = array() ;
    foreach(Aplicacion::model()->findAll() as $app){
       $apps[$app->id] = $app->nombre_app;
    }
$memberFormConfig = array(
      'elements'=>array(
        'nombre'=>array(
            'type'=>'text',
            'maxlength'=>250,
        ),
        'apellido'=>array(
            'type'=>'text',
            'maxlength'=>250,
        ),
        'email'=>array(
            'type'=>'text',
            'maxlength'=>250,
        ),
        'password'=>array(
            'type'=>'text',
            'maxlength'=>250,
        ),
    ));

$this->widget('MultiModelForm',array(
        'id' => 'id_member', //the unique widget id
        'formConfig' => $memberFormConfig, //the form configuration array
        'model' => $member, //instance of the form model
        'tableView' => true,
 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedMembers,
 
        //array of member instances loaded from db
        //'data' => $member->findAll('groupid=:groupId', array(':groupId'=>$model->id)),
    ));
?>

<br>
Asociarlos a aplicaciones:
<div class="row">
    <?php  
        $data = array();
        $apps = array() ;
         foreach(Aplicacion::model()->findAll() as $app){
            $apps[$app->id] = $app->nombre_app;
        }
        echo Chosen::multiSelect('Aplicaciones',$data , $apps,array('style'=>'width:100%'));
        
    
    ?>
</div>
 
<div class="row buttons">
    <?php echo CHtml::submitButton('Guardar'); ?>
</div>
 
<?php $this->endWidget(); ?>
 
</div><!-- form -->