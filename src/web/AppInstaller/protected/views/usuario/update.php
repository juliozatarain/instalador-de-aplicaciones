<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->nombre.' '.$model->apellido=>array('view','id'=>$model->id),
	'Editar',
);
?>

<h1>Editar usuario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>