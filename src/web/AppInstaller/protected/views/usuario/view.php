<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->nombre.' '.$model->apellido,
);
?>

<h1>Usuario</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                'id',
		'nombre',
		'apellido',
		'email',
		'password',
		'password_t',
	),
)); ?>

<br>
Aplicaciones:
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>true,
)); ?>
<div class="row">
    <?php  
        $data = array();
        foreach($model->aplicaciones as $app){
            array_push($data,$app->id);
        }
        $apps = array() ;
         foreach(Aplicacion::model()->findAll() as $app){
            $apps[$app->id] = $app->nombre_app;
        }
        echo Chosen::multiSelect('Aplicaciones',$data , $apps,array('style'=>'width:100%'));
        echo CHtml::submitButton('Guardar');
    
    ?>
</div>
<?php $this->endWidget(); ?>
