<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Bienvenido a <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>En el menu superior se encuentran las opciones principales.</p>

<br>

<?php
$columns = array('App','Nombre','Ultima actualizacion','Cliente','Ver','Agregar version');
$rows = array();
foreach($data as $app){
    $arr = array(
        CHtml::image('http://sisu.mx/development/kantorovich/AppInstaller/index.php/rest/556391-9555/img/'.$app->idApp,$app->nombre,array('width'=>'40', 'height'=>'40')),
        $app->nombre,
        $app->ultima_ver,
        $app->cliente,
        CHtml::link(CHtml::image("http://sisu.mx/development/kantorovich/AppInstaller/assets/img/view.png"),array('aplicacion/view','id'=>$app->idApp)),
        CHtml::link(CHtml::image("http://sisu.mx/development/kantorovich/AppInstaller/assets/img/edit.png"),array('version/createAppVer','aplicacion_id'=>$app->idApp)),
        );
    array_push($rows,$arr);
}

$this->widget('htmlTableUi',array(
    'ajaxUrl'=>'site/handleHtmlTable',
    'arProvider'=>'',    
    'collapsed'=>false,
    'columns'=>$columns,
    'cssFile'=>'',
    'editable'=>false,
    'enableSort'=>true,
    'rows'=>$rows,
    'sortColumn'=>2,
    'sortOrder'=>'desc',
    'title'=>'Aplicaciones recientes:',
));
?>


