<?php
/* @var $this DispositivoController */
/* @var $model Dispositivo */

$this->breadcrumbs=array(
	'Dispositivos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dispositivo', 'url'=>array('index')),
	array('label'=>'Create Dispositivo', 'url'=>array('create')),
	array('label'=>'Update Dispositivo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dispositivo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dispositivo', 'url'=>array('admin')),
);
?>

<h1>View Dispositivo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'token',
		'plataforma',
		'usuario_id',
	),
)); ?>
