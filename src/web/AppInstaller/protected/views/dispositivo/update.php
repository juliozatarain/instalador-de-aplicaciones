<?php
/* @var $this DispositivoController */
/* @var $model Dispositivo */

$this->breadcrumbs=array(
	'Dispositivos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Dispositivo', 'url'=>array('index')),
	array('label'=>'Create Dispositivo', 'url'=>array('create')),
	array('label'=>'View Dispositivo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Dispositivo', 'url'=>array('admin')),
);
?>

<h1>Update Dispositivo <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>