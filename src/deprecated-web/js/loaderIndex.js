//Librerías
document.write('<script type="text/javascript" src="js/libs/cordova-2.7.0.js"></script>');
document.write('<script type="text/javascript" src="js/libs/zepto.min.js"></script>');
document.write('<script type="text/javascript" src="js/libs/touch.js"></script>');
document.write('<script type="text/javascript" src="js/libs/iscroll-lite.js"></script>');
document.write('<script type="text/javascript" src="js/libs/md5lib.js"></script>');


//Helpers
document.write('<script type="text/javascript" src="js/helpers/Validacion.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Notificacion.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/NotificacionesPush.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Fecha.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Strings.js"></script>');
document.write('<script type="text/javascript" src="js/helpers/Loading.js"></script>');




//Dependientes de la plataforma
if (navigator.userAgent.match(/(Android)/))   //Android
{    
    //Helpers
    document.write('<script type="text/javascript" src="js/helpers/AndroidInstaller.js"></script>');
    
    //Libs
    document.write('<script type="text/javascript" src="js/libs/GCMPlugin.js"></script>');
    document.write('<script type="text/javascript" src="js/libs/webintent.js"></script>');
    
}
else if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)){

    //Libs
    document.write('<script type="text/javascript" src="js/libs/PushNotification.js"></script>');
}



//Modelos
document.write('<script type="text/javascript" src="js/models/Conexion.js"></script>');
document.write('<script type="text/javascript" src="js/models/Sesion.js"></script>');



//Vistas
document.write('<script type="text/javascript" src="js/views/usuario/CambioPasswordView.js"></script>');
document.write('<script type="text/javascript" src="js/views/usuario/SolicitaCambioPasswordView.js"></script>');
document.write('<script type="text/javascript" src="js/views/version/DetalleVersionView.js"></script>');
document.write('<script type="text/javascript" src="js/views/aplicacion/ListaAplicacionesView.js"></script>');
document.write('<script type="text/javascript" src="js/views/version/ListaVersionView.js"></script>');
document.write('<script type="text/javascript" src="js/views/usuario/LoginView.js"></script>');



//Controladores
document.write('<script type="text/javascript" src="js/controllers/UsuarioController.js"></script>');
document.write('<script type="text/javascript" src="js/controllers/AplicacionesController.js"></script>');




var actualController = {'action':null,'params':[]};

//Se espera a que cargue cordova
window.addEventListener('load',function(){
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) 
    {
        document.addEventListener("deviceready", onDeviceReady, false);
    }
    else
    {
        $(document).ready(function()
        {
            onDeviceReady();
        });
    }
});
    
function onDeviceReady() 
{
    //cuando se detecta el click fuera dle teclado se hace blur para esconder el teclado
    $('body').on("tap",  function escuchadorDeTouch(e)
    {
        if(e.target.tagName != 'INPUT' && e.target.tagName != 'SELECT' && e.target.tagName != 'TEXTAREA')
        {
            document.activeElement.blur();
        }
    });
    $(document).on("ajaxStart", function()
    {
        activarEstadoDecarga();

    }).on("ajaxStop", function()
    {
        desactivarEstadoDecarga();
    });   
    if (navigator.userAgent.match(/(Android)/))
    {
        registroPushNotif(null);
    }

    document.addEventListener('push-notification', function(event) 
    {
            // Could pop an alert here if app is open and you still wanted to see your alert
            navigator.notification.alert(event.notification.aps.alert, null, "AppInstaller", "Aceptar");
            refresh();

    });

    var usuario = new UsuarioController();
    usuario.index();
}

var duracionFadeIn = 500;
function animaVista(animacionTerminada)
{
     $("body").animate(
        {
          opacity: 1.0
        }, duracionFadeIn, 'ease-in', animacionTerminada());
}

