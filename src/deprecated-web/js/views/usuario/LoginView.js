function LoginView()
{
	this.view="";
	this.view += "            <div class=\"logoSuperior\">";
	this.view += "                <img src=\"images\/logo.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"titulo\">";
	this.view += textos.nombreApp;
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorFormaLogin\">";
	this.view += "                <form action=\"\" onsubmit=\"new UsuarioController().login();return false;\">";
	this.view += "                    <div class=\"contenedorCampo a1\">";
	this.view += "                        <div class=\"contenedorInternoLabel\">";
	this.view += "                            <div class=\"centradorVerticalTexto\">";
	this.view += textos.loginUsuario;
	this.view += "                            <\/div>";
	this.view += "                        <\/div>";
	this.view += "                        <div class=\"contenedorInternoTextfield\">";
	this.view += "                            <input type=\"text\" name=\"email\" id =\"email\" >";
	this.view += "                        <\/div>                  ";
	this.view += "                    <\/div>";
	this.view += "";
	this.view += "                    <div class=\"contenedorCampo a1\">";
	this.view += "                        <div class=\"contenedorInternoLabel\">";
	this.view += "                            <div class=\"centradorVerticalTexto\">";
	this.view += textos.loginPassword;
	this.view += "                            <\/div>";
	this.view += "                        <\/div>";
	this.view += "                        <div class=\"contenedorInternoTextfield\">";
	this.view += "                            <input type=\"password\" name=\"clave\" id=\"clave\">";
	this.view += "                        <\/div> ";
	this.view += "                    <\/div>";
	this.view += "";
	this.view += "                    <div class=\"contenedorOlvidePassword\">";
	this.view += textos.loginOlvideMiPassword;
	this.view += "                    <\/div>   ";
	this.view += "                    <div id=\"botonL\" class=\"botonLogin\">";
	this.view += "                        <div class=\"textoBoton\">";
	this.view += textos.loginLogin;
	this.view += "                        <\/div>";
	this.view += "                    <\/div>               ";
	this.view += "                    <input type=\"submit\" style=\"visibility:hidden;width:0.5px;height:0.5px;\"/>";
	this.view += "                <\/form>";
	this.view += "            <\/div>";

}

LoginView.prototype.loadView = function()
{

	   var obj=this.datos;
	   $("body").css("opacity","0");	   
	   $('#home').html(this.view);
	   animaVista(transitionReady);

       function transitionReady()
       {	
		   //binding de los eventos touch del botón
		   $('div.botonLogin').on("touchstart", function()
		   {
		   		$(this).css('background-image','url(images/btn-login_pressed.png)');
		   });
		   $('div.botonLogin').on("touchmove", function()
		   {
		   		$(this).css('background-image','url(images/btn-login.png)');
		   });
		   $('div.botonLogin').on("touchend", function()
		   {
		   		$(this).css('background-image','url(images/btn-login.png)');
		   });
		   $('div.botonLogin').tap(function()	
		   {
		   		var usuarioController = new UsuarioController();
		   		usuarioController.login();
		   });
		   //binding de los eventos touch de olvide password
		   $('div.contenedorOlvidePassword').on("touchstart", function()
		   {
		   		$(this).css('color','#005288');
		   });
		   $('div.contenedorOlvidePassword').on("touchmove", function()
		   {
		   		$(this).css('color','grey');
		   });
		   $('div.contenedorOlvidePassword').on("touchend", function(e)
		   {
		   		$(this).css('color','grey');
		   		var vistaSolicitaCambioPasswordView = new SolicitaCambioPasswordView();
		   		vistaSolicitaCambioPasswordView.loadView();
				 e.stopPropagation();
				 e.preventDefault();
		   });
		}

}