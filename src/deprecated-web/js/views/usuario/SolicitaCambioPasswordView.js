function SolicitaCambioPasswordView()
{
	this.view="";
	this.view += "<div class=\"botonRegresar\">";
	this.view += "                <img src=\"images\/back_btn.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"logoSuperior\">";
	this.view += "                <img src=\"images\/logo.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"titulo\">";
	this.view += textos.nombreApp;
	this.view += "            <\/div>";
	this.view += "            <div class=\"mensajeOlvide\">";
	this.view += textos.siOlvidaste;
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorFormaLoginOlvide\">";
	this.view += "                <form action=\"\" onsubmit=\"new UsuarioController().olvidePassword();return false;\">";
	this.view += "                    <div class=\"contenedorCampo\">";
	this.view += "                        <div class=\"contenedorTextfieldOlvide\">";
	this.view += "                            <input  type=\"text\" name=\"email\" id =\"email\" >";
	this.view += "                        <\/div>                  ";
	this.view += "                    <\/div>";
	this.view += "                    <div id=\"botonL\" class=\"botonLogin\">";
	this.view += "                        <div class=\"textoBoton\">";
	this.view += textos.loginEnviar;
	this.view += "                        <\/div>";
	this.view += "                    <\/div>               ";
	this.view += "                    <input type=\"submit\" style=\"visibility:hidden;width:0.5px;height:0.5px;\"/>";
	this.view += "                <\/form>";
	this.view += "            <\/div>";
}

SolicitaCambioPasswordView.prototype.loadView = function()
{
	var obj=this.datos;
   $("body").css("opacity","0");	   
   $('#home').html(this.view);
   animaVista(transitionReady);

   function transitionReady()
   {	
		 //binding de los eventos touch del botón
		$('div.botonLogin').on("touchstart", function()
		{
				$(this).css('background-image','url(images/btn-login_pressed.png)');
		});
		$('div.botonLogin').on("touchmove", function()
		{
				$(this).css('background-image','url(images/btn-login.png)');
		});
		$('div.botonLogin').on("touchend", function()
		{
				$(this).css('background-image','url(images/btn-login.png)');
		});
		$('div.botonLogin').tap(function()	
		{
				var usuarioController = new UsuarioController();
		   		usuarioController.olvidePassword();
		});
		 //binding de los eventos touch del boton de regresar
		$('div.botonRegresar').on("touchstart", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn_pressed.png");
		});
		$('div.botonRegresar').on("touchmove", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn.png");
		});
		$('div.botonRegresar').on("touchend", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn.png");
		});
		$('div.botonRegresar').tap(function()
		{
			var vistaLogin = new LoginView();
			vistaLogin.loadView();
		});
	}
}