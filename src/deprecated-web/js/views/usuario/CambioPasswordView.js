function CambioPasswordView()
{
	this.view="";
	this.view += "            <div class=\"logoSuperior\">";
	this.view += "                <img src=\"images\/logo.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"titulo\">";
	this.view += textos.nombreApp;
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorFormaLoginOlvide\">";
	this.view += "                <form action=\"\" onsubmit=\"new UsuarioController().cambioPassword();return false;\">";
	this.view += "                    <div class=\"contenedorCampo cambio\">";
	this.view += "                        <div class=\"contenedorTextfieldOlvide\">";
	this.view += "                            <strong>"+textos.loginIntroduceNuevoPassword+"</strong>";
	this.view += "                        <\/div>                  ";
	this.view += "                    <\/div>";
	this.view += "                    <div class=\"contenedorCampo cambio\">";
	this.view += "                        <div class=\"contenedorTextfieldOlvide\">";
	this.view += "                            <input type=\"password\" name=\"clave1\" id =\"clave1\" >";
	this.view += "                        <\/div>                  ";
	this.view += "                    <\/div>";
	this.view += "                    <br/>";
	this.view += "                    <div class=\"contenedorCampo cambio\">";
	this.view += "                        <div class=\"contenedorTextfieldOlvide\">";
	this.view += "                            <strong>"+textos.loginRepetirNuevoPassword+"</strong>";
	this.view += "                        <\/div>                  ";
	this.view += "                    <\/div>";
	this.view += "                    <div class=\"contenedorCampo cambio\">";
	this.view += "                        <div class=\"contenedorTextfieldOlvide\">";
	this.view += "                            <input type=\"password\" name=\"clave2\" id =\"clave2\" >";
	this.view += "                        <\/div>";
	this.view += "                    <\/div>";
	this.view += "                    <div id=\"botonL\" class=\"botonLogin fixboton\">";
	this.view += "                        <div class=\"textoBoton\">";
	this.view += textos.loginEnviar;
	this.view += "                        <\/div>";
	this.view += "                    <\/div>  ";
	this.view += "                    <input type=\"submit\" style=\"visibility:hidden;width:0.5px;height:0.5px;\"/>";
	this.view += "                <\/form>";
	this.view += "            <\/div>";

}

CambioPasswordView.prototype.loadView = function()
{
	var obj=this.datos;
	$("body").css("opacity","0");	   
	$('#home').html(this.view);
	animaVista(transitionReady);

	function transitionReady()
	{	
		//binding de los eventos touch del botón
		$('div.botonLogin').on("touchstart", function()
		{
			$(this).css('background-image','url(images/btn-login_pressed.png)');
		});
		$('div.botonLogin').on("touchmove", function()
		{
			$(this).css('background-image','url(images/btn-login.png)');
		});
		$('div.botonLogin').on("touchend", function()
		{
			$(this).css('background-image','url(images/btn-login.png)');
		});
		$('div.botonLogin').tap(function()	
		{
			var usuarioController = new UsuarioController();
			usuarioController.cambioPassword();
		});
	}
}