function ListaAplicacionesView(data)
{

       var  this.view="";
			this.view += "<ul class=\"list-view right-one-icons\">";
			this.view += "    <li>";
			this.view += "        <a href=\"#\" class=\"content\">";
			this.view += "        	<h2>Acme 1,200 Widgets<\/h2>";
			this.view += "            <p>";
			this.view += "                <strong>$24,000<\/strong><br\/>";
			this.view += "                Value Proposition";
			this.view += "            <\/p>";
			this.view += "            <div class=\"list-view-icons\">";
			this.view += "                <span class=\"icon-right-arrow\">&nbsp;<\/span>";
			this.view += "            <\/div>";
			this.view += "        <\/a>";
			this.view += "    <\/li>";
			this.view += "    <li>";
			this.view += "        <a href=\"#\" class=\"content\">";
			this.view += "            <h2>100 Servers<\/h2>";
			this.view += "            <p>";
			this.view += "                <strong class=\"value\">$45,000<\/strong><br\/>";
			this.view += "                Needs Analysis";
			this.view += "            <\/p>";
			this.view += "            <div class=\"list-view-icons\">";
			this.view += "                <span class=\"icon-right-arrow\">&nbsp;<\/span>";
			this.view += "            <\/div>";
			this.view += "        <\/a>";
			this.view += "    <\/li>";
			this.view += "<\/ul>";

}

ListaAplicacionesView.prototype.loadView = function()
{
	   

	   var obj=this.datos;
	   $("body").css("opacity","0");	   
	   $('#home').html(this.view);
	   animaVista(transitionReady);

       function transitionReady()
       {	
			/* Inicio Se calcula y se asigna el tamaño del scroller */
			var hijos  = $('#home').children();	
			var tamTotal=0; 
			for(var x=0; x<hijos.length;x++)
			{
				tamTotal+=$(hijos[x]).height();
			}
			var tamFinal = Math.floor((window.screen.height - (tamTotal -$('#contenedorLista').height()))/86) * 86 + "px";
			$('#contenedorLista').css('max-height',tamFinal);
			/* Fin Se calcula y se asigna el tamaño del scroller */

			var myScrollApps = new iScroll('contenedorLista', { bounce: false, snap: 'li', momentum: false});  
			$('div.contenedorBotonLogout img').tap(function()
			{
					var usuarioController = new UsuarioController();
			   		usuarioController.cerrarSesion();
			});
			$('div.contenedorBotonLogout img').on("touchstart",function()
			{
				$("div.contenedorBotonLogout img").attr("src","images/btn_logout_pressed.png");

			});
			$('div.contenedorBotonLogout img').on("touchmove",function()
			{
				$("div.contenedorBotonLogout img").attr("src","images/btn_logout.png");

			});
			$('div.contenedorBotonLogout img').on("touchend",function()
			{
				$("div.contenedorBotonLogout img").attr("src","images/btn_logout.png");

			});
		        
		        //Binding de cada app
		        for(var i=0;i<obj.apps.length;i++){
		            $('#'+obj.apps[i].idApp).tap(function()
		            {
		                    var vistaListaVersion = new AplicacionesController();
		                    vistaListaVersion.listaVersiones($(this).attr('id'));
		                    $(this).parent().css("background-color","lightgrey");

		            });
		        }
		}
	
}