function DetalleVersionView(data)
{ 

    this.datos = data;
	this.view="";
	this.view += "            <div id=\"fondoOscurecedor\">";
	this.view += "            <\/div>";
	this.view += "            <div class=\"popBack\">";
	this.view += "            <div class=\"textoNotas\">";
	this.view += "                <p>Agrega un comentario:<\/p>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorTextoComentario\">";
	this.view += "            	<textarea  rows=\"4\" cols=\"50\" id=\"comentario\">"+textos.escribeComentario+"</textarea>";
	this.view += "            <\/div>";
	this.view += "            <div id=\"botonEnviarComentario\" class=\"botonEnviarComentario\">";
	this.view += "                <img src=\"images\/btn-install.png\"\/>";
	this.view += "                    <div class=\"botonDescargarTexto\">"+textos.enviarComentario+"<\/div>";
	this.view += "            <\/div>";
	this.view += "            <\/div>";
	this.view += "  <div class=\"botonRegresar\">";
	this.view += "                <img src=\"images\/back_btn.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"logoyTextoSuperiorMini\">";
	this.view += "                <div class=\"contenedorLogoMini\">";
	this.view += "                <\/div>";
	this.view += "                <div class=\"tituloPrincipalMini\">";
	this.view += "                    <div class=\"centradorVerticalTituloMini\">";
	this.view += "                        App Installer";
	this.view += "                    <\/div>";
	this.view += "                <\/div>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorElementoLista\">";
	this.view += "                <div class=\"elementoLista\">";
	this.view += "                    <div class=\"contenedorImagenElementoLista\">";
	this.view += "                        <img src=\"http://sisu.mx/development/kantorovich/AppInstaller/index.php/rest/"+data.token+"/img/"+data.app.idApp+"\">";
	this.view += "                    <\/div>";
	this.view += "                    <div class=\"descripcionElementoLista\">";
	this.view += "                       <p><strong>"+data.app.nombre+"<\/strong><\/p>";
	this.view += "                       <p class=\"ultimaVersion\">Versión: "+data.version.version+"<\/p>";
	this.view += "                       <p class=\"fechaListaMini\">"+formatoFecha(data.version.fecha_version)+"<\/p>";
	this.view += "                    <\/div>";
	this.view += "                <\/div>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"textoNotas\">";
	this.view += "                <p>Notas de la versión<\/p>";
	this.view += "            <\/div>";
	this.view += "            <div id=\"contenedorTextoNotas\" class=\"contenedorTextoNotas\">";
	this.view += "                <p>"+data.version.notas+"<\/p>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"abrirAgregarComentario\">";
	this.view += "                <div class=\"botonDescargarTexto agr\">Agregar Comentario<\/div>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"botonDescargar\">";
	this.view += "                <img src=\"images\/btn-install.png\"\/>";
	this.view += "                    <div class=\"botonDescargarTexto\">"+textos.instalar+"<\/div>";
	this.view += "            <\/div>";
}

DetalleVersionView.prototype.loadView = function()
{
                     
   var obj=this.datos;
   $("body").css("opacity","0");	   
   $('#home').html(this.view);
   animaVista(transitionReady);
   function transitionReady()
	{	    	
		var myScrollNotas = new iScroll('contenedorTextoNotas', { bounce: false, momentum: false});  

		 //binding de los eventos touch del boton de regresar
		$('div.botonRegresar').on("touchstart", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn_pressed.png");
		});
		$('div.botonRegresar').on("touchmove", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn.png");
		});
		$('div.botonRegresar').on("touchend", function()
		{
			$("div.botonRegresar img").attr("src","images/back_btn.png");
		});
		$('div.botonRegresar').tap(function()
		{
			var controlador = new AplicacionesController();
			controlador.listaVersiones(obj.app.idApp);
		});
	    $('div.botonDescargar').tap(function()
	    {
	        var controlador = new AplicacionesController();
	        controlador.descargarAplicacion(obj.app.idApp,obj.version.version);
	    });
	    $('div.botonDescargar').on("touchstart",function()
	    {			
	    	$('div.botonDescargar img').attr("src","images/btn-install_pressed.png");
	    });
	    $('div.botonDescargar').on("touchmove",function()
	    {			
	    	$('div.botonDescargar img').attr("src","images/btn-install.png");
	    });
	    $('div.botonDescargar').on("touchend",function()
	    {			
	    	$('div.botonDescargar img').attr("src","images/btn-install.png");
	    });
	    $('div.abrirAgregarComentario').on("touchstart",function()
	    {			
	    	$('div.abrirAgregarComentario').css("background-color","grey");
	    });
	    $('div.abrirAgregarComentario').on("touchmove",function()
	    {			
	    	$('div.abrirAgregarComentario').css("background-color","lightgrey");
	    });
	    $('div.abrirAgregarComentario').on("touchend",function()
	    {			
	    	$('div.abrirAgregarComentario').css("background-color","lightgrey");
	    });
	    $('div.abrirAgregarComentario').tap(function()
	    {			
		   mostrarPopUp();
		   $("textarea").on("focus",function() 
		        {
				    if( $(this).val() == textos.escribeComentario ) 
				    {
				        $(this).val("");
					}
				});
        });


        $('div.botonEnviarComentario').on("touchstart",function()
	    {			
	    	$('div.botonEnviarComentario img').attr("src","images/btn-install_pressed.png");
	    });
	    $('div.botonEnviarComentario').on("touchmove",function()
	    {			
	    	$('div.botonEnviarComentario img').attr("src","images/btn-install.png");
	    });
	    $('div.botonEnviarComentario').on("touchend",function()
	    {			
	    	$('div.botonEnviarComentario img').attr("src","images/btn-install.png");
	    });
	    $('div.botonEnviarComentario').tap(function()
		{
			var controlador = new AplicacionesController();
			controlador.enviarComentario(obj.app.idApp,obj.version.version,esconderPopUp);
		});
		$('#fondoOscurecedor').on("touchstart",function()
		{
			if(document.activeElement.getAttribute('id') != 'comentario')
			{
				esconderPopUp();
			}
		});

	   function mostrarPopUp()
	   {
	   		 $("#fondoOscurecedor").animate(
	        {
	          opacity: 0.5
	        }, 200, 'ease-in', activarPointerF);
	   		 function activarPointerF()
	   		 {
	   		 	$("#fondoOscurecedor").css("pointer-events","auto");
	   		 }
	   		 $(".popBack").animate(
	        {
	          opacity: 1.0
	        }, 200, 'ease-in', activarPointerP);
	   		 function activarPointerP()
	   		 {
	   		 	$(".popBack").css("pointer-events","auto");
	   		 	$("div.botonEnviarComentario").css("pointer-events","auto");
	   		 	$("textarea#comentario").css("pointer-events","auto");
	   		 }
	   }
	   function esconderPopUp()
	   {
	   		 $("#fondoOscurecedor").animate(
	        {
	          opacity: 0.0
	        }, 200, 'ease-in', desactivarPointerF);
	   		 function desactivarPointerF()
	   		 {
	   		 	$("#fondoOscurecedor").css("pointer-events","none");
	   		 }
	   		 $(".popBack").animate(
	        {
	          opacity: 0.0
	        }, 200, 'ease-in', desactivarPointerP);
	   		 function desactivarPointerP()
	   		 {
	   		 	$(".popBack").css("pointer-events","none");
	   		 	$("div.botonEnviarComentario").css("pointer-events","none");
	   		 	$("textarea#comentario").css("pointer-events","none");


	   		 }
	   }	    
	}
}