function ListaVersionView(data)
{
        this.datos = data;
	this.view="";
	this.view += " <div class=\"botonRegresar\">";
	this.view += "                <img src=\"images\/back_btn.png\"\/>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"logoyTextoSuperiorMini\">";
	this.view += "                <div class=\"contenedorLogoMini\">";
	this.view += "                <\/div>";
	this.view += "                <div class=\"tituloPrincipalMini\">";
	this.view += "                    <div class=\"centradorVerticalTituloMini\">";
	this.view += textos.nombreApp;
	this.view += "                    <\/div>";
	this.view += "                <\/div>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"saludoSubtitulo\">";
	this.view += "                <p>Bienvenido "+data.nombre+"<\/p>";
	this.view += "                <p class=\"saltoPeq\"><strong>"+textos.versiones+"<\/strong><\/p>";
	this.view += "            <\/div>";
	this.view += "            <div class=\"contenedorLista\" id=\"contenedorLista\">";
	this.view += "                <ul class=\"lista\">";
        for(var i=0;i<data.versiones.length;i++)
        {
            this.view += "                    <li>";
            this.view += "                        <div class=\"elementoLista\" id=\""+i+"\">";
            this.view += "                            <div class=\"contenedorImagenElementoLista\">";
            this.view += "                                <img src=\""+urls.appURL+"AppInstaller/index.php/rest/"+data.token+"/img/"+data.app.idApp+"\">";;
            this.view += "                            <\/div>";
            this.view += "                            <div class=\"descripcionElementoLista\">";
            this.view += "                               <p><strong>"+data.app.nombre+"<\/strong><\/p>";
            this.view += "                               <p class=\"ultimaVersion\">"+textos.versiontx+" "+data.versiones[i].version+"<\/p>";
            this.view += "                               <p class=\"fechaLista\">"+formatoFecha(data.versiones[i].fecha_version)+"<\/p>";
            this.view += "                            <\/div>";
            this.view += "                        <\/div>";
            this.view += "                    <\/li>";
        }
	this.view += "                <\/ul>";
	this.view += "            <\/div>";
}

ListaVersionView.prototype.loadView = function()
{
	   var obj=this.datos;
	   $("body").css("opacity","0");	   
	   $('#home').html(this.view);
	   animaVista(transitionReady);

       function transitionReady()
       {	
			/* Se calcula y se asigna el tamño del scroller */
			var hijos  = $('#home').children();	
			var tamTotal=0; 
			for(var x=0; x<hijos.length;x++)
			{
				tamTotal+=$(hijos[x]).height();
			}
			var tamFinal = Math.floor((window.screen.height - (tamTotal -$('#contenedorLista').height()))/86) * 86 + "px";
			$('#contenedorLista').css('max-height',tamFinal);
			/* Se calcula y se asigna el tamño del scroller */
			var myScroll = new iScroll('contenedorLista', { bounce: false, snap: 'li', momentum: false});

		    //binding de los eventos touch del boton de regresar
			$('div.botonRegresar').on("touchstart", function()
			{
				$("div.botonRegresar img").attr("src","images/back_btn_pressed.png");
			});
			$('div.botonRegresar').on("touchmove", function()
			{
				$("div.botonRegresar img").attr("src","images/back_btn.png");
			});
			$('div.botonRegresar').on("touchend", function()
			{
				$("div.botonRegresar img").attr("src","images/back_btn.png");
			});
			$('div.botonRegresar').tap(function()
			{
		   		var controlador = new AplicacionesController();
				controlador.listaAplicaciones();
			});
		    //Binding de cada version
		    for(var i=0;i<obj.versiones.length;i++)
		    {
		        $('#'+i).tap(function()
		        {
		                var controlador = new AplicacionesController();
		                controlador.detalleVersion(obj.app.idApp,obj.versiones[$(this).attr('id')].version);
		                $(this).parent().css("background-color","lightgrey");

		        });
		    }
		}
}