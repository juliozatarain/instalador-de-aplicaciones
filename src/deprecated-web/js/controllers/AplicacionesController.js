
function AplicacionesController()
{
}

AplicacionesController.prototype.listaAplicaciones = function()
{
        actualController = {'action':AplicacionesController.prototype.listaAplicaciones,'params':[]};
        var sesion = new Sesion().get();
        var token = sesion.token;
    
	var conexion = new Conexion();
        conexion.put(
                        callbackAppList,
                        urls.appURL+'AppInstaller/index.php/rest/appList',
                        {'token':token}
                    );
                        
        function callbackAppList(datos)
        {
            if(datos['resultado'] === 0)
            {
                
                var view= new ListaAplicacionesView({'nombre':sesion.nombre,'apps':datos['aplicaciones'],'token':token});
                view.loadView();
                
            }
            else if(datos['resultado'] === 1)
            {
                sesion.token=null;
                new Sesion().set(sesion);
                var view= new LoginView();
                view.loadView();
            }
            else
            {
                //Error
            }
        }
}

AplicacionesController.prototype.listaVersiones = function(idApp)
{
        actualController = {'action':AplicacionesController.prototype.listaVersiones,'params':[idApp]};
        var sesion = new Sesion().get();
        var token = sesion.token;
    
	var conexion = new Conexion();
        conexion.put(
                        callbackVerList,
                        urls.appURL+'AppInstaller/index.php/rest/verList',
                        {
                            'token':token,
                            'idApp':idApp
                        }
                    );
                        
        function callbackVerList(datos)
        {
            
            if(datos['resultado'] === 0)
            {
                
                var data = {'nombre':sesion.nombre,'token':token,'app':datos['aplicacion'],'versiones':datos['versiones']};
                
                var view= new ListaVersionView(data);
                view.loadView();
                
            }
            else if(datos['resultado'] === 1)
            {
                sesion.token=null;
                new Sesion().set(sesion);
                var view= new LoginView();
                view.loadView();
            }
            else
            {
                //Error
            }
        }
}

AplicacionesController.prototype.detalleVersion = function(idApp,version)
{
        actualController = {'action':null,'params':[]};
        var sesion = new Sesion().get();
        var token = sesion.token;
    
	var conexion = new Conexion();
        conexion.put(
                        callbackVerList,
                        urls.appURL+'AppInstaller/index.php/rest/version',
                        {
                            'token':token,
                            'idApp':idApp,
                            'version':version
                        }
                    );
                        
        function callbackVerList(datos)
        {
            
            if(datos['resultado'] === 0)
            {
                
                var data = {'nombre':sesion.nombre,'token':token,'app':datos['aplicacion'],'version':datos['version']};
                
                var view= new DetalleVersionView(data);
                view.loadView();
                
            }
            else if(datos['resultado'] === 1)
            { 
                sesion.token=null;
                new Sesion().set(sesion);
                var view= new LoginView();
                view.loadView();
            }
            else
            {
                //Error
            }
        }
}
	
AplicacionesController.prototype.descargarAplicacion = function(idApp,version)
{
    actualController = {'action':null,'params':[]};
    var sesion = new Sesion().get();
    var token = sesion.token;
    
    if (navigator.userAgent.match(/(Android)/))   //Android
    {    
        var appInst = new AndroidInstaller(token,idApp,version);
        fileTransfer = appInst.DownloadAndInstall();
        $('.botonDescargar').unbind();
        fileTransfer.onprogress = function(progressEvent) {
                if (progressEvent.lengthComputable) {
                    var porcentaje = ((progressEvent.loaded/progressEvent.total)*50).toFixed(2);
                    $('.botonDescargarTexto').text('Descargando...'+porcentaje+'%');
                }
            };

    }
    else if (navigator.userAgent.match(/(iPhone|iPod|iPad)/))
    {
        window.location = 'itms-services://?action=download-manifest&url='+urls.appURL+'AppInstaller/index.php/rest/'+token+'/plist/'+idApp+'/'+version;
    }
}

AplicacionesController.prototype.enviarComentario = function(idApp,version,callback)
{
    if($('#comentario').val().length>0 && $('#comentario').val() != textos.escribeComentario)
   { 
        var sesion = new Sesion().get();
        var token = sesion.token;
        var conexion = new Conexion();
            conexion.put(
                            callbackEnviarComentario,
                            urls.appURL+'AppInstaller/index.php/rest/comentario',
                            {
                                'token':token,
                                'idApp':idApp,
                                'version':version,
                                'comentario':$('#comentario').val()
                            }
                        );
                            
            function callbackEnviarComentario(datos)
            {
                
                if(datos['resultado'] === 0)
                {
                    muestraNotificacion(textos.comentarioEnviado, callbacknotif, "AppInstaller", "Aceptar");
                    function callbacknotif()
                    {
                        callback();
                    }
                }
                else if(datos['resultado'] === 1)
                { 
                    sesion.token=null;
                    new Sesion().set(sesion);
                    var view= new LoginView();
                    view.loadView();
                }
                else
                {
                    muestraNotificacion(textos.fallaComentario, null, "AppInstaller", "Aceptar");
                }
            }
    }
    else
    {
        muestraNotificacion(textos.introduzcaComentario, null, "AppInstaller", "Aceptar");
    }
}
