
function UsuarioController()
{
	this.sesion = new Sesion();

}

UsuarioController.prototype.index = function()
{

	//si hay sesión se carga la lista de aplicaciones
	if(this.sesion.get()!=null)
	{

		new AplicacionesController().listaAplicaciones();
	}
	else // si no hay una sesión almacenada se carga login
	{
                var vista= new LoginView();
                vista.loadView();
	}
}

UsuarioController.prototype.login = function()
{

	//validamos
        var mensajeError="";
	var validador = new Validacion([
            [$('#email'), '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$', textos.validacionUsuarioInvalido]
            ]);
        if($('#email').val().length<1)
        {
            mensajeError=textos.validacionIntroduceUsuario;
        }
        else if ($('#clave').val().length<1)
        {
            mensajeError=textos.validacionIntroducePassword;
        }
        else if(!validador.validar())
        {
            mensajeError=validador.mensajeError;
        }
        else 
        {
            var correo = $('#email').val();
            var clave = $('#clave').val();
            
            //Aqui obtenemos token con el helper NotificacionesPush.js
            registroPushNotif(tokenCallback);
            
            function tokenCallback(token)
            {
                var plataforma = null;
                if (navigator.userAgent.match(/(Android)/))   //Android
                {    
                    plataforma = 1;
                }
                else if (navigator.userAgent.match(/(iPhone|iPod|iPad)/))   //iOS
                {  
                    plataforma = 0;
                }
                var datosAEnviar = {'correo':$('#email').val(), 'clave':hex_md5(String($('#clave').val())),'token':token,'plataforma':plataforma};

                var conexion = new Conexion();
                conexion.put(
                                callbackAppList,
                                urls.appURL+'AppInstaller/index.php/rest/login',
                                datosAEnviar
                            );

                function callbackAppList(datos)
                {
                    if(datos.resultado == 0)
                    {
                        new Sesion().set({'idUsuario':datos.idUsuario,'token':token, 'nombre':datos.nombre+" "+datos.apellido, 'correo':correo, 'clave':clave});

                        if(!datos.cambio || datos.cambio == "false")
                        {
                            var dum = new AplicacionesController();
                            dum.listaAplicaciones();  
                        }
                        else 
                        {
                            var vistaCambio = new CambioPasswordView();
                            vistaCambio.loadView();
                        }

                    }
                    else
                    {
                        muestraNotificacion(textos.autentificacionAccesoDenegado, null, textos.nombreApp, textos.loginAceptar);

                    }

                }
            }
        }
        if(mensajeError.length>0)
        {      
            muestraNotificacion(mensajeError, null, "AppInstaller", "Aceptar");
        }
}

UsuarioController.prototype.olvidePassword = function()
{

    var mensajeError="";
    var validador = new Validacion([
            [$('#email'), '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$', textos.validacionUsuarioInvalido]
            ]);
    if($('#email').val().length<1)
    {
        mensajeError="Introduce un correo";
    }
    else if(!validador.validar())
    {
        mensajeError=validador.mensajeError;
    }
    else 
    {
        var datosAEnviar = {'correo':$('#email').val()}
        var conexion = new Conexion();
        conexion.put(
                        callbackAppList,
                        urls.appURL+'AppInstaller/index.php/rest/cambiarPassword',
                        datosAEnviar
                    );
                        
        function callbackAppList(datos)
        {
            if(datos.resultado==0)
            {

                muestraNotificacion(textos.nuevoPasswordEnviado, cerrarSesionCallback, textos.nombreApp, textos.loginAceptar);

                function cerrarSesionCallback()
                {
                    var vista= new LoginView();
                    vista.loadView();
                }           
            }
            else
            {
                muestraNotificacion(textos.falloCambioPassword, null, textos.nombreApp, textos.loginAceptar);

            }

        }

    }
    if(mensajeError.length>0)
    {      
        muestraNotificacion(mensajeError, null, textos.nombreApp, textos.loginAceptar);
    }
}

UsuarioController.prototype.cambioPassword = function()
{
	var mensajeError="";
    if($('#clave1').val().length<1)
    {
        mensajeError=textos.loginIntroduceNuevoPassword;
    }
    else if($('#clave1').val()!= $('#clave2').val())
    {
        mensajeError=textos.validacionPasswordsNoCoinciden;
    }
    else 
    {   
        var sesion = new Sesion();
        var datosAEnviar = {'correo':sesion.get().correo, 'clave':hex_md5(String(sesion.get().clave)), 'nuevaClave':hex_md5(String($('#clave1').val())), 'nuevaClave2':hex_md5(String($('#clave2').val()))}
        var conexion = new Conexion();
        conexion.put(
                        callbackAppList,
                         urls.appURL+'AppInstaller/index.php/rest/nuevoPassword',
                        datosAEnviar
                    );
                        
        function callbackAppList(datos)
        {
            if(datos.resultado==0)
            {

                muestraNotificacion(textos.exitoCambioPassword, notificacionCallback, textos.nombreApp, textos.loginAceptar);

                function notificacionCallback()
                {
                    var vista= new LoginView();
                    vista.loadView();
                }           
            }
            else
            {
                muestraNotificacion(textos.falloCambioPassword, null, textos.nombreApp, textos.loginAceptar);

            }

        }

    }
    if(mensajeError.length>0)
    {      
        muestraNotificacion(mensajeError, null, textos.nombreApp, textos.loginAceptar);
    }
}

UsuarioController.prototype.cerrarSesion = function()
{
    muestraConfirmacion(textos.advertenciaLogout, logoutCallback,  textos.nombreApp, [textos.loginCancelar,textos.loginAceptar]);
    function logoutCallback(button)
    {
        if(button == 2)
        {
            var sesion= new Sesion();
    var datosAEnviar = {'token':sesion.get().token};
    var conexion = new Conexion();
    conexion.put(
                    callbackAppList,
                    urls.appURL+'AppInstaller/index.php/rest/logout',
                    datosAEnviar
                );
                    
    function callbackAppList(datos)
    {
        if(datos.resultado==0)
        {
            muestraNotificacion(textos.sesionCerrada, cerrarSesionCallback, textos.nombreApp, textos.loginAceptar);
            
            function cerrarSesionCallback()
            {
                var vista= new LoginView();
                vista.loadView();
            }

        }
        else
        {
            muestraNotificacion(textos.sesionNoCerrada, null, textos.nombreApp, textos.loginAceptar);

        }

    }
            
            
        }
        
    }
    
	
}