function AndroidInstaller(token,idApp,version)
{
    this.fileTransfer = new FileTransfer();
    this.token = token;
    this.idApp = idApp;
    this.version = version;
    
}

AndroidInstaller.prototype.DownloadAndInstall = function()
{
    var obj = this;
    window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(fileSystem)
        {
                fileSystem.root.getFile('tmp.apk', 
                        {
                            create: true, 
                            exclusive: false
                        }
                        , function(fileEntry) 
                        {
                            
                            var localPath = fileEntry.fullPath;
                            obj.fileTransfer.download("http://sisu.mx/development/kantorovich/AppInstaller/index.php/rest/"+obj.token+"/app/"+obj.idApp+"/"+obj.version, localPath, function(entry) 
                                {
                                    window.plugins.webintent.startActivity(
                                        {
                                            action: window.plugins.webintent.ACTION_VIEW,
                                            url: 'file://' + entry.fullPath,
                                            type: 'application/vnd.android.package-archive',
                                        },
                                        function(){},
                                        function(e)
                                        {
                                            alert('Error launching app update');
                                        }
                                    );                              

                                }
                                , function (error) 
                                {
                                    alert("Error downloading APK: " + error.code);
                                });
                            }
                            , function(evt)
                            {
                                alert("Error downloading apk: " + evt.target.error.code);                                               
                            }
                        );
        }
        , function(evt)
        {
        alert("Error preparing to download apk: " + evt.target.error.code);
        }
    );
        
    return obj.fileTransfer;    
}

            