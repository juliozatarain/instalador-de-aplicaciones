var urls = {
	appURL : "http://sisu.mx/development/kantorovich/"
};
var textos = 
{
    nombreApp: "App Installer",
    loginIngresaUsuario: "Ingresa tu usuario:",
    loginRegresar: "Regresar",
    loginAceptar: "Aceptar",
    loginCancelar: "Cancelar",
    loginIntroduceNuevoPassword: "Introduce un nuevo password.",
    loginNuevoPassword : "Nuevo password:",
    loginRepetirNuevoPassword : "Repeite nuevo password:",
    loginEnviar : "Enviar",
    loginUsuario: "usuario:",
    loginPassword: "password:",
    loginOlvideMiPassword : "¿Olvidaste tu Password?",
    loginLogin : "Login",
    loginBienvenido : "Bienvenido",

    
    conexionErrorNoInternet: {"titulo":"Conexión", "mensaje":"No hay conexión a internet."},
    conexionErrorServidor: {"titulo":"Conexión", "mensaje":"Error al cargar la información."},
    conexionErrorTimeout :  {"titulo":"Conexión", "mensaje":"Ha pasado el tiempo de espera."},
    conexionErrorDesconocido :  {"titulo":"Conexión", "mensaje":"Error desconocido"},
    
    autentificacionAccesoDenegado:"Acceso Denegado",
    autentificacionSesionFinalizada: {"titulo":"Sesión", "mensaje":"Tu sesion ha finalizado"}, 
    autentificacionSesionFinalizadaApp: {"titulo":"Sesión", "mensaje":"Tu sesion ha finalizado por inactividad"},
    
    validacionPasswordInvalido : "Password inv&aacute;lido.",
    validacionIntroducePassword : "Introduce un password",
    validacionUsuarioInvalido : "Usuario inv&aacute;lido",
    validacionIntroduceUsuario : "Introduce un usuario",
    validacionPasswordsNoCoinciden : "Los passwords no coinciden.",

    nuevoPasswordEnviado : "Se ha enviado un correo con el nuevo password",
    falloCambioPassword : "Ha fallado la solicitud de cambio de password",
    exitoCambioPassword :"Se ha cambiado tu password",
    advertenciaLogout : "¿Cerrar sesión?",
    sesionCerrada: "Se ha cerrado la sesión",
    sesionNoCerrada: "No se ha cerrado la sesión",

    tusApps:"Tus aplicaciones:",
    ultimaVersion:"Última versión:",
    siOlvidaste:"Si olvidaste tu password, teclea tu correo aquí:",
    versiones:"Versiones:",
    versiontx:"Versión",
    instalar:"Instalar",
    comentarioEnviado:"Se ha enviado tu comentario.",
    enviarComentario:"Enviar Comentario",
    escribeComentario:"Escribe tu comentario aquí...",
    introduzcaComentario:"Introduzca un comentario.",
    fallaComentario:"No fue posible enviar tu comentario."



};
