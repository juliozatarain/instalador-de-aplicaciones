var tokenRecCallback = null;

function registroPushNotif(tokenCallback)
{
    tokenRecCallback = tokenCallback;
    
    if(navigator.userAgent.match(/(iPhone|iPod|iPad)/))   //iOS
    {    
        window.plugins.pushNotification.registerDevice({alert:true, badge:true, sound:true}, 

            function(status) {

                                        tokenCallback(status.deviceToken);
                             });

    }
    else if(navigator.userAgent.match(/(Android)/))   //Android
    {   
        //window.GCM.unregister( GCM_Success, GCM_Fail );
        window.plugins.GCM.register("208709640995", "GCM_Event", GCM_Success, GCM_Fail );
        
    }
}

function GCM_Success(e){}
function GCM_Fail(e){ tokenRecCallback(null); }
function GCM_Event(e)
{
    switch( e.event )
    {
        case 'registered':
            tokenRecCallback(e.regid);
            break;
        case 'message':
            if(actualController.action !== null){
                refresh();
            }
            break;
        default:
            muestraNotificacion('EVENT -> '+e.event , null, "Contacto", "Aceptar");
            break;
    }

}
function refresh()
{
    actualController.action.apply(this,actualController.params);

}