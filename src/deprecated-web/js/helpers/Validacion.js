function Validacion(validationRules)
{
    this.validationRules = validationRules;
    this.mensajeError = "ERRORES: \n";
}

Validacion.prototype = 
{
    validar : function()
    {
        if(this.validationRules != null)
        {
            var unoNoValido = false;
            for(var i = 0; i < this.validationRules.length; i++)
            {
                var regex = new RegExp(this.validationRules[i][1],"i");
                var valido = regex.test(this.validationRules[i][0].val());
                if(!valido)
                {
                    this.mensajeError += this.validationRules[i][2]+'\n';
                    unoNoValido = true;
                    break;
                }
            }
            if(unoNoValido)
                return false;
            else
                return true;
        }
        else
        {
            return false;
        }
    }
}
