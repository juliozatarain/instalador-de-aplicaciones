/*
 * Regresa fecha en formato:
 * Miercoles 17 de julio de 2013
 * 
 * @param {Date} fecha
 * @return {String} parsedDate
 * 
 */
function formatoFecha(fecha)
{
    var date;
    if(navigator.userAgent.match(/(iPhone|iPod|iPad)/))   //iOS
    {
        var dat = fecha.split('-');
        date = new Date(Number(dat[0]),Number(dat[1]),Number(dat[2]));
    }    
    else
        date = new Date(fecha);
    var year = date.getFullYear().toString();
    var month = (date.getMonth());
    var day = date.getDate().toString();
    var weekDay = date.getDay();
    
    var monthNames = [ "enero", "febrero", "marzo", "abril", "mayo", "junio",
    "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" ];
    var dayNames = [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
    "Sábado" ];

    var parsedDate = dayNames[weekDay] + ' ' + day + ' de ' + monthNames[month] + ' de ' + year;
    return parsedDate;
                
}