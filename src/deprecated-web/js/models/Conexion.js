/**
 * Crea una instancia de Conexion
 * @constructor
 * @this {Conexion}
 */
function Conexion()
{   
}

Conexion.prototype = 
{
    /**
     * Llama un servicio web en una dirección específica y el resultado lo manda como parámetro a la función callback especificada
     * @param {function} callbackFunctionParam La función callback que se ejecutará al tener éxito la obtención del recurso
     * @param {String} urlParam La dirección del servicio web
     */ 
    fetch: function(callbackFunctionParam, urlParam)
    {
        var obj = this;
        try
        {
            $.ajax
            (
            {
                type: "get",
                dataType: "json",
                url: urlParam,
                success: function(data) 
                {
                    callbackFunctionParam(data);
                },
                error: function(jqXHR, exception)
                {
                    obj.errorCatch(jqXHR, exception);
                }
            }
            );
        }
        catch(ex)
        {
            alert(ex.description);
        }
    },

    /**
     * Manda un recurso en formato JSON a un servicio web en una dirección específica y el resultado lo manda como parámetro a la función callback especificada
     * @param {function} callbackFunctionParam La función callback que se ejecutará al tener éxito la obtención del recurso
     * @param {string} urlParam La dirección del servicio web
     * @param {object} dataParam Los datos en formato JSON que será enviado por POST al servicio web
     */
    put : function(callbackFunctionParam, urlParam, dataParam)
    {
        var obj = this;
        $.ajax
        (
        {
            type: "post",
            data: dataParam,
            dataType: "json",
            url: urlParam,
            success: function(data) 
            {
                callbackFunctionParam(data);
            },
            error: function(jqXHR, exception)
            {
                obj.errorCatch(jqXHR, exception);
            }
        }
        );
    },

    /**
     * Determina que error sucedio en caso de fracasar la conexion
     * @param {objeto} jqXHR El objeto de respuesta
     * @param {string} exception El mensaje de excepcion
     */
    errorCatch: function(jqXHR, exception) 
    {

        if (jqXHR.status === 0) 
        {
            muestraNotificacion(textos.conexionErrorNoInternet.mensaje, null, textos.conexionErrorNoInternet.titulo, 'Aceptar');

        }
        else if (jqXHR.status == 404) 
        {
            muestraNotificacion(textos.conexionErrorServidor.mensaje, null, textos.conexionErrorServidor.titulo, 'Aceptar');
        }
        else if (jqXHR.status == 500) 
        {
            muestraNotificacion(textos.conexionErrorServidor.mensaje, null, textos.conexionErrorServidor.titulo, 'Aceptar');
        }
        else if (exception === 'parsererror') 
        {
            muestraNotificacion(textos.conexionErrorServidor.mensaje, null, textos.conexionErrorServidor.titulo, 'Aceptar');
        }
        else if (exception === 'timeout') 
        {
            muestraNotificacion(textos.conexionErrorTimeout.mensaje, null, textos.conexionErrorTimeout.titulo, 'Aceptar');
        }
        else if (exception === 'abort') 
        {
            muestraNotificacion(textos.conexionErrorServidor.mensaje, null, textos.conexionErrorServidor.titulo, 'Aceptar');
        }
        else 
        {
            muestraNotificacion(textos.conexionErrorDesconocido.mensaje + jqXHR.responseText, null, textos.conexionErrorDesconocido.titulo, 'Aceptar');
        }
    }
};