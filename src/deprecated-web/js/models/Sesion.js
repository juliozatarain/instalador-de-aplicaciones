/**
 * Crea una instancia de Sesion
 * @constructor
 * @this {Sesion}
 */
function Sesion()
{
}

Sesion.prototype =
{
    /**
     * Almacena localmente en la ventana la sesion del usuario
     * @param {JSON} dataParam El JSON de datos de sesion que se almacenan en la ventana
     */
    set: function (dataParam)
    {
        window.localStorage.setItem("sesion", JSON.stringify(dataParam));
    },
    /**
     * Recupera la sesion del usuario del almacenamiento local de la ventana
     * @return {JSON} Sesion del usuario
     */
    get: function ()
    {
        if(window.localStorage.getItem("sesion") != null)
            return JSON.parse(window.localStorage.getItem("sesion"));
        else
            return null;
    }
}