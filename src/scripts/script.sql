CREATE TABLE usuarios(
	id int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(250) NOT NULL,
	apellido varchar(250) NOT NULL,
	email varchar(250) NOT NULL,
	password varchar(250) NOT NULL,
	password_t varchar(250), PRIMARY KEY (id));

CREATE TABLE aplicaciones(
	id int(10) NOT NULL AUTO_INCREMENT,
	nombre varchar(250) NOT NULL,
	nombre_app varchar(250) NOT NULL,
	version int(10) NOT NULL,
	loc_profile int(10) NOT NULL, PRIMARY KEY (id));
	
CREATE TABLE usuarios_aplicaciones(
usuario_id int(11) NOT NULL,
 aplicacion_id int(10) NOT NULL,
 PRIMARY KEY (usuario_id, aplicacion_id));

CREATE TABLE dispositivos(
	id int(10) NOT NULL AUTO_INCREMENT,
	token varchar(250) NOT NULL,
	udid varchar(250),
	plataforma int(10) NOT NULL,
	usuario_id int(11) NOT NULL,
	PRIMARY KEY (id));
	 
CREATE TABLE versiones(
	id int(10) NOT NULL AUTO_INCREMENT, 
	aplicacion_id int(10) NOT NULL, 
	version varchar(250) NOT NULL, 
	notas varchar(500) NOT NULL, 
	fecha_version date NOT NULL, 
	PRIMARY KEY (id));
	
ALTER TABLE versiones 
	ADD INDEX FKversiones683155 (aplicacion_id), 
	ADD CONSTRAINT FKversiones683155 FOREIGN KEY (aplicacion_id) REFERENCES aplicaciones (id);
	
ALTER TABLE usuarios_aplicaciones 
	ADD UNIQUE INDEX FKusuarios_a493773 (aplicacion_id), 
	ADD CONSTRAINT FKusuarios_a493773 FOREIGN KEY (aplicacion_id) REFERENCES aplicaciones (id);
	
ALTER TABLE usuarios_aplicaciones 
	ADD INDEX FKusuarios_a380688 (usuario_id), 
	ADD CONSTRAINT FKusuarios_a380688 FOREIGN KEY (usuario_id) REFERENCES usuarios (id);
	
ALTER TABLE dispositivos 
	ADD INDEX FKdispositiv363409 (usuario_id), 
	ADD CONSTRAINT FKdispositiv363409 FOREIGN KEY (usuario_id) REFERENCES usuarios (id);
